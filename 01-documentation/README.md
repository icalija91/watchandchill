# Aplikacija za predlaganje filmova i tv serija

Ovo je projekat za ispit iz predmeta Praktikum - Internet i veb tehnologije

Broj indeksa: 2019203784

Ime i prezime: Igor Calija

Skolska godina: 2022/2023

## Projektni zahtev

Aplikacija treba da omogući administratoru da se prijavi sa svojim pristupnim parametrima i da uređuje spisak serija i filmova, žanrova i oznaka (tagova). Filmovi pripadaju jednoj kategoriji i jednom ili više žanrova, imaju sliku koja ih ilustruje, originalni naziv, prevedeni naziv na srpski, ime režisera, sinopsis, kao i popis oznaka (tagova) koje najbliže određuju sadržaj filma. Serije imaju iste podatke kao i film, s tim da imaju takođe i epizode koje imaju svoj broj sezone i broj epizode u sezoni. Svaka epizoda može da ima svoju posebnu sliku, naziv i opis, kao i spisak oznaka (tagova) koje najbliže određuju njen konkretan sadržaja. Korisnici se registruju na aplikaciju i prijavljuju sa registrovanim pristupnim parametrima. Korisnik može da obeleži film, seriju ili epizodu jednim od sledećih statusa: gledao, želi da gleda ili ne želi da gleda. Ako je obeležio sa gledao, onda može da oceni film/seriju/epizodu ocenom od 1 do 10. Na osnovu podataka o ocenama filmova, serija i epizoda serija i oznaka (tagova) tih obeleženih filmova/serija/epizoda, aplikacija korisniku prikazuje spisak preporučenih filmova i serija, poređanih po njihovim prosečnim ocenama na osnovu ocena ostalih korisnika, s tim da treba dodatno svaku ocenu drugih korisnika vrednovati više ili manje na osnovu toga da li je ocena data od strane korisnika koji češće ocenjuje filmove/serije/epizode sa oznakama (tagovima) koje postoje u konkretnom filmu/seriji/oceni na koju se ocena odnosi. Samostalno osmisliti algoritam za vrednovanje pojedinačnih ocena, ali tako da uzimaju u obzir oznake (tagove), prosečne ocene, ukupan broj ocena i ranije preference korisnika. Preference korisnika ustanoviti na osnovu toga koje tagove imaju serije i filmovi koje je obeležio da je gledao i koje je pozitivno ocenio, ali uzeti u obzir da određene tegove treba diskriminisati u listi ako pripadaju serijama i filmovima koje je obeležio da ne želi da gleda ili kojima je dao niske ocene. Grafički interfejs sajta treba da bude realizovan sa responsive dizajnom.

## Tehnicka ogranicenja

- Aplikacija mora da bude realizovana na Node.js platformi korišćenjem Express biblioteke. Aplikacija mora da bude podeljena u dve nezavisne celine: back-end veb servis (API) i front-end (GUI aplikacija). Sav kôd aplikacije treba da bude organizovan u jednom Git spremištu u okviru korisničkog naloga za ovaj projekat, sa podelom kao u primeru zadatka sa vežbi.
- Baza podataka mora da bude relaciona i treba koristiti MySQL ili MariaDB sistem za upravljanje bazama podataka (RDBMS) i u spremištu back-end dela aplikacije mora da bude dostupan SQL dump strukture baze podataka, eventualno sa inicijalnim podacima, potrebnim za demonstraciju rada projekta.
- Back-end i front-end delovi projekta moraju da budi pisani na TypeScript jeziku, prevedeni TypeScript prevodiocem na adekvatan JavaScript. Back-end deo aplikacije, preveden na JavaScript iz izvornog TypeScript koda se pokreće kao Node.js aplikacija, a front-end deo se statički servira sa rute statičkih resursa back-end dela aplikacije i izvršava se na strani klijenta. Za postupak provere identiteta korisnika koji upućuje zahteve back-end delu aplikacije može da se koristi mehanizam sesija ili JWT (JSON Web Tokena), po slobodnom izboru.
- Sav generisani HTML kôd koji proizvodi front-end deo aplikacije mora da bude 100% validan, tj. da prođe proveru W3C Validatorom (dopuštena su upozorenja - Warning, ali ne i greške - Error). Grafički korisnički interfejs se generiše na strani klijenta (client side rendering), korišćenjem React biblioteke, dok podatke doprema asinhrono iz back-end dela aplikacije (iz API-ja). Nije neophodno baviti se izradom posebnog dizajna grafičkog interfejsa aplikacije, već je moguće koristiti CSS biblioteke kao što je Bootstrap CSS biblioteka. Front-end deo aplikacije treba da bude realizovan tako da se prilagođava različitim veličinama ekrana (responsive design).
- Potrebno je obezbediti proveru podataka koji se od korisnika iz front-end dela upućuju back-end delu aplikacije. Moguća su tri sloja zaštite i to: (1) JavaScript validacija vrednosti na front-end-u; (2) Provera korišćenjem adekvatnih testova ili regularnih izraza na strani servera u back-end-u (moguće je i korišćenjem izričitih šema - Schema za validaciju ili drugim pristupima) i (3) provera na nivou baze podataka korišćenjem okidača nad samim tabelama baze podataka.
- Neophodno je napisati prateću projektnu dokumentaciju o izradi aplikacije koja sadrži (1) model baze podataka sa detaljnim opisom svih tabela, njihovih polja i relacija; (2) dijagram baze podataka; (3) dijagram organizacije delova sistema, gde se vidi veza između baze, back-end, front-end i korisnika sa opisom smera kretanja informacija; (4) popis svih aktivnosti koje su podržane kroz aplikaciju za sve uloge korisnika aplikacije prikazane u obliku Use-Case dijagrama; kao i (5) sve ostale elemente dokumentacije predviđene uputstvom za izradu dokumentacije po ISO standardu.
- Izrada oba dela aplikacije (projekata) i promene kodova datoteka tih projekata moraju da bude praćene korišćenjem alata za verziranje koda Git, a kompletan kôd aplikacije bude dostupan na javnom Git spremištu, npr. na besplatnim GitHub ili Bitbucket servisima, jedno spremište za back-end projekat i jedno za front-end projekat. Ne može ceo projekat da bude otpremljen u samo nekoliko masovnih Git commit-a, već mora da bude pokazano da je projekat realizovan u kontinuitetu, da su korišćene grane (branching), da je bilo paralelnog rada u više grana koje su spojene (merging) sa ili bez konflikata (conflict resolution).

## Pristupni parametri

### **Administrator:**

username: administrator \
password: Password1

### **User:**

username: test@test.com \
password: Password1
## Baza podataka

## Model baze podataka

### Tabela movie

![Dijagram baze](./img/tables/movie.png)

### Tabela movie-user

![Dijagram baze](./img/tables/movie-user.png)

### Tabela movie-tag

![Dijagram baze](./img/tables/movie-tag.png)

### Tabela movie-genre

![Dijagram baze](./img/tables/movie-genre.png)

### Tabela tvshow

![Dijagram baze](./img/tables/tvshow.png)

### Tabela tvshow-user

![Dijagram baze](./img/tables/tvshow-user.png)

### Tabela tvshow-tag

![Dijagram baze](./img/tables/tvshow-tag.png)

### Tabela episode

![Dijagram baze](./img/tables/episode.png)

### Tabela episode-user

![Dijagram baze](./img/tables/episode-user.png)

### Tabela episode-tag

![Dijagram baze](./img/tables/episode-tag.png)

### Tabela tag

![Dijagram baze](./img/tables/tag.png)

### Tabela genre

![Dijagram baze](./img/tables/genre.png)

### Tabela user

![Dijagram baze](./img/tables/user.png)

### Tabela administrator

![Dijagram baze](./img/tables/administrator.png)
## Dijagram baze podataka

![Dijagram baze](./img/er-diagram.png)

## Use-case dijagram

![Use-case dijagram](./img/use-case.jpg)

### Uloge korisnika

**Administrator**

- Filmovi:
  - Pregled svih filmova
  - Dodavanje filma
  - Menjanje filma
  - Brisanje filma

- Serije:
  - Pregled svih serija
  - Dodavanje serije
  - Menjanje serije
  - Brisanje serije

- Epizoda:
  - Pregled svih epizoda
  - Dodavanje epizode
  - Menjanje epizode
  - Brisanje epizode

- Zanrovi:
  - Pregled svih zanrova
  - Dodavanje zanra
  - Menjanje zanra
  - Brisanje zanra

- Tagovi:
  - Pregled svih tagova
  - Dodavanje taga
  - Menjanje taga
  - Brisanje taga

- Korisnici:
  - Pregled svih korisnika
  - Dodavanje korisnika
  - Menjanje sifre korisniku
  - Brisanje korisnika

- Administratori:
  - Pregled svih administratora
  - Dodavanje administratora
  - Menjanje sifre administratoru
  - Brisanje administratora  

- Odjava

**Korisnik**

- Filmovi:
  - Pregled preporucenih filmova
  - Pregled svih filmova
  - Ocenjivanje filma
  - Postavljanje statusa filmu

- Serije:
  - Pregled preporucenih serija
  - Pregled svih serija
  - Ocenjivanje serija
  - Postavljanje statusa seriji

- Epizoda:
  - Pregled svih epizoda
  - Ocenjivanje epizode
  - Postavljanje statusa epizodi
- Izmena sifre
- Odjava

**Posetilac**

- Logovanje
  - kao administrator
  - kao korisnik
- Registracija
