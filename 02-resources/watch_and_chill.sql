-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: watch_and_chill
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `administrator` (
  `administrator_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password_hash` varchar(128) NOT NULL,
  PRIMARY KEY (`administrator_id`),
  UNIQUE KEY `uq_administrator_username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrator`
--

LOCK TABLES `administrator` WRITE;
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` VALUES (2,'administrator','$2b$10$C9taw/0mKYZM.MMDrWHwteheXvYWLSvTC.RT4Fd0dJe0qMP9FGqea'),(3,'adminone','$2b$10$BPS1vkL1rnUNQvE68/8LduQE8ewW5f6P93e8tGBynep.f2V2WWxEC');
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `episode`
--

DROP TABLE IF EXISTS `episode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `episode` (
  `episode_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tv_show_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `number` int(11) NOT NULL,
  `synopsis` varchar(512) NOT NULL,
  `image_url` varchar(512) NOT NULL,
  `season_number` int(10) unsigned NOT NULL,
  `episode_number` int(10) unsigned NOT NULL,
  PRIMARY KEY (`episode_id`),
  KEY `fk_episode_tv_show_id` (`tv_show_id`) USING BTREE,
  CONSTRAINT `fk_episode_tv_show_id` FOREIGN KEY (`tv_show_id`) REFERENCES `tv_show` (`tv_show_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `episode`
--

LOCK TABLES `episode` WRITE;
/*!40000 ALTER TABLE `episode` DISABLE KEYS */;
INSERT INTO `episode` VALUES (4,3,'Mr Bean',1,'Absurd comedy about a likeable buffoon who turns the most ordinary situations into moments of excruciating embarrassment. Mr Bean rushes to his exam only to find out he\'s studied the wrong subject. He then goes to the beach but struggles to change into his swimming trunks discretely. Later on, he has difficulty attending a church service without drawing attention to himself.','https://www.themoviedb.org/t/p/original/pUNQV35kDaKuejnXGNiUeEBurNE.jpg',1,14),(9,5,'1:23:45',1,'Plant workers and firefighters put their lives on the line to control a catastrophic explosion at a Soviet nuclear power plant in 1986. An early-morning explosion at the nuclear power plant sends workers scrambling to assess the damage.','https://m.media-amazon.com/images/I/91qFtxYzkcL.jpg',1,5),(10,6,'Chapter One: The Vanishing of Will Byers',1,'At the U.S. Dept. of Energy an unexplained event occurs. Then when a young Dungeons and Dragons playing boy named Will disappears after a night with his friends, his mother Joyce and the town of Hawkins are plunged into darkness.','https://pbs.twimg.com/media/FXtXMO2UUAEmywE.jpg',1,8),(11,10,'Diamond of the First Water',1,'Daphne Bridgerton debuts on London\'s marriage market as a new gossip sheet sets high society atwitter and Simon, the eligible Duke of Hastings, returns to town.','http://images6.fanpop.com/image/photos/43600000/-Bridgerton-Season-1-poster-bridgerton-netflix-series-43618653-1080-1350.jpg',1,8),(12,11,'El Jefe',1,'Three decades of relative calm are shattered when an act of stoned stupidity unleashes Deadite mayhem back into the life of braggart Ash Williams.','https://m.media-amazon.com/images/M/MV5BMTYyMjQyNTE5MF5BMl5BanBnXkFtZTgwMjEyMjE2NDM@._V1_.jpg',1,10),(13,12,'Uno',1,'Jimmy works magic in the courtroom, and after being inspired unexpectedly, Jimmy tries an unconventional method for pursuing potential clients.','https://flxt.tmsimg.com/assets/p10492760_b_v8_aa.jpg',1,10),(14,13,'Episode 1',1,'Birmingham gangster Thomas Shelby sees an opportunity to move up in the world when a crate of guns goes missing.','https://m.media-amazon.com/images/I/71mXmB41psS._AC_UF894,1000_QL80_.jpg',1,6),(15,14,'Pilot',1,'Joe meets and falls in love with Beck and goes down a social-media rabbit hole to learn everything about her; while this may be a real shot at real love, there are some things standing in the way, such as her ex, Benji.','https://flxt.tmsimg.com/assets/p15725175_b1t_v9_aa.jpg',1,9),(16,5,'Please remain calm',2,'With untold millions at risk, nuclear physicist Ulana Khomyuk makes a desperate attempt to reach Valery Legasov, a leading Soviet nuclear physicist, and warn him about the threat of a second explosion that could devastate the continent.','https://mir-s3-cdn-cf.behance.net/project_modules/2800_opt_1/6ff248143548387.627c6503033a0.png',1,5),(17,15,'Pilot',1,'The vampires are hard at work trying to organise a lavish blood feast in honour of their ancient master, who is visiting all the way from the Old Country.','https://flxt.tmsimg.com/assets/p16522742_b1t_v10_aa.jpg',1,10),(18,16,'Name of the Game',1,'When a Supe kills the love of his life, A/V salesman Hughie Campbell teams up with Billy Butcher, a vigilante hell-bent on punishing corrupt Supes, and Hughie\'s life will never be the same again.','https://imageio.forbes.com/specials-images/imageserve/5d30f849eab9270008f9c359/The-Boys-Poster/960x0.jpg?format=jpg&width=960',1,8);
/*!40000 ALTER TABLE `episode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `episode_tag`
--

DROP TABLE IF EXISTS `episode_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `episode_tag` (
  `episode_tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `episode_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`episode_tag_id`),
  KEY `fk_episode_tag_tag_id` (`tag_id`),
  KEY `fk_episode_tag_episode_id` (`episode_id`),
  CONSTRAINT `fk_episode_tag_episode_id` FOREIGN KEY (`episode_id`) REFERENCES `episode` (`episode_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_episode_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `episode_tag`
--

LOCK TABLES `episode_tag` WRITE;
/*!40000 ALTER TABLE `episode_tag` DISABLE KEYS */;
INSERT INTO `episode_tag` VALUES (15,10,9),(16,1,9),(17,7,4),(18,3,10),(20,11,11),(21,7,12),(22,3,12),(23,10,13),(24,1,13),(25,10,14),(26,10,15),(27,10,16),(28,7,17),(29,3,17),(30,7,18),(31,10,18);
/*!40000 ALTER TABLE `episode_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `episode_user`
--

DROP TABLE IF EXISTS `episode_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `episode_user` (
  `episode_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `episode_id` int(10) unsigned NOT NULL,
  `status` enum('watched','want_to_watch','dont_want_to_watch') DEFAULT NULL,
  `rating` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`episode_user_id`),
  UNIQUE KEY `uq_episode_user_user_id_episode_id` (`user_id`,`episode_id`) USING BTREE,
  KEY `fk_episode_user_episode_id` (`episode_id`),
  CONSTRAINT `fk_episode_user_episode_id` FOREIGN KEY (`episode_id`) REFERENCES `episode` (`episode_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_episode_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `episode_user`
--

LOCK TABLES `episode_user` WRITE;
/*!40000 ALTER TABLE `episode_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `episode_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `genre` (
  `genre_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`genre_id`),
  UNIQUE KEY `uq_genre_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (3,'Action'),(2,'Comedy'),(4,'Drama'),(9,'Horror'),(5,'Thriller');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `movie` (
  `movie_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `serbian_title` varchar(128) NOT NULL,
  `director` varchar(32) NOT NULL,
  `synopsis` varchar(512) NOT NULL,
  `image_url` varchar(512) NOT NULL,
  `category` enum('artistic','indie','box-office','documentary','silent') NOT NULL,
  PRIMARY KEY (`movie_id`),
  KEY `uq_movie_title_serbian_title` (`title`,`serbian_title`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (28,'Schindlers List','Sindlerova lista',' Steven Spielberg','In German-occupied Poland during World War II, industrialist Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazis.','https://m.media-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_FMjpg_UX1000_.jpg','box-office'),(32,'Your Name','Tvoje ime','Makoto Shinkai','Two strangers find themselves linked in a bizarre way. When a connection forms, will distance be the only thing to keep them apart?','https://m.media-amazon.com/images/I/61KYVvWl-LL._AC_UF894,1000_QL80_.jpg','artistic'),(34,'Silent Hill','Tiha dolina','Christophe Gans','A woman, Rose, goes in search for her adopted daughter within the confines of a strange, desolate town called Silent Hill.','https://m.media-amazon.com/images/I/71A-Ej3zI3L._AC_UF894,1000_QL80_.jpg','silent'),(35,'Beetlejuice','Bitljus','Tim Burton','The spirits of a deceased couple are harassed by an unbearable family that has moved into their home, and hire a malicious spirit to drive them out.','https://m.media-amazon.com/images/I/71Zrz2YSDVL.jpg','box-office'),(36,'The Hangover','Mamurluk u Vegasu','Todd Phillips','Three buddies wake up from a bachelor party in Las Vegas, with no memory of the previous night and the bachelor missing. They make their way around the city in order to find their friend before his wedding.','https://m.media-amazon.com/images/I/519rvPBZQPL._AC_UF894,1000_QL80_.jpg','box-office'),(37,'Spiderman: No Way Home','Spajdermen',' Jon Watts','With Spider-Man\'s identity now revealed, Peter asks Doctor Strange for help. When a spell goes wrong, dangerous foes from other worlds start to appear, forcing Peter to discover what it truly means to be Spider-Man.','https://m.media-amazon.com/images/M/MV5BZWMyYzFjYTYtNTRjYi00OGExLWE2YzgtOGRmYjAxZTU3NzBiXkEyXkFqcGdeQXVyMzQ0MzA0NTM@._V1_FMjpg_UX1000_.jpg','box-office'),(38,'The Dark Knight','Crni Vitez','Christopher Nolan','When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.','https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_FMjpg_UX1000_.jpg','box-office'),(39,'Jocker','Dzoker','Todd Phillips','A mentally troubled stand-up comedian embarks on a downward spiral that leads to the creation of an iconic villain.','https://m.media-amazon.com/images/I/71KPOvu-hOL.jpg','box-office'),(40,'Silver Linings Playbook','U dobru i u zlu','David O. Russell','After a stint in a mental institution, former teacher Pat Solitano moves back in with his parents and tries to reconcile with his ex-wife.','https://m.media-amazon.com/images/M/MV5BMTM2MTI5NzA3MF5BMl5BanBnXkFtZTcwODExNTc0OA@@._V1_.jpg','box-office'),(41,'Tusk','Kljova','Kevin Smith','A brash and arrogant podcaster gets more than he bargained for when he travels to Canada to interview a mysterious recluse... who has a rather disturbing fondness for walruses.','https://m.media-amazon.com/images/M/MV5BNmQyMmUzMmYtMTA4OS00ZmRjLWE0NWYtNjc0ZGM0N2E4YzQ5XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg','box-office');
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_genre`
--

DROP TABLE IF EXISTS `movie_genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `movie_genre` (
  `movie_genre_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `genre_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`movie_genre_id`),
  KEY `fk_movie_genre_genre_id` (`genre_id`),
  KEY `fk_movie_genre_movie_id` (`movie_id`),
  CONSTRAINT `fk_movie_genre_genre_id` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`genre_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_movie_genre_movie_id` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_genre`
--

LOCK TABLES `movie_genre` WRITE;
/*!40000 ALTER TABLE `movie_genre` DISABLE KEYS */;
INSERT INTO `movie_genre` VALUES (64,2,35),(65,4,28),(70,3,38),(71,4,39),(72,4,40),(73,3,37),(74,4,37),(75,4,35),(76,5,32),(77,9,34),(78,2,36),(79,5,39),(80,9,41);
/*!40000 ALTER TABLE `movie_genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_tag`
--

DROP TABLE IF EXISTS `movie_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `movie_tag` (
  `movie_tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`movie_tag_id`),
  KEY `fk_movie_tag_tag_id` (`tag_id`),
  KEY `fk_movie_tag_movie_id` (`movie_id`),
  CONSTRAINT `fk_movie_tag_movie_id` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_movie_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_tag`
--

LOCK TABLES `movie_tag` WRITE;
/*!40000 ALTER TABLE `movie_tag` DISABLE KEYS */;
INSERT INTO `movie_tag` VALUES (23,11,32),(27,7,35),(28,3,35),(29,1,28),(33,1,37),(34,1,38),(37,11,40),(39,3,34),(40,7,36),(41,10,39),(42,10,28),(43,7,41),(44,3,41);
/*!40000 ALTER TABLE `movie_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_user`
--

DROP TABLE IF EXISTS `movie_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `movie_user` (
  `movie_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `movie_id` int(10) unsigned NOT NULL,
  `status` enum('watched','want_to_watch','dont_want_to_watch') DEFAULT NULL,
  `rating` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`movie_user_id`),
  UNIQUE KEY `uq_movie_user_user_id_movie_id` (`user_id`,`movie_id`),
  KEY `fk_movie_user_movie_id` (`movie_id`),
  CONSTRAINT `fk_movie_user_movie_id` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`movie_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_movie_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_user`
--

LOCK TABLES `movie_user` WRITE;
/*!40000 ALTER TABLE `movie_user` DISABLE KEYS */;
INSERT INTO `movie_user` VALUES (46,8,28,'watched',8),(48,8,34,'dont_want_to_watch',1),(49,8,35,NULL,3),(54,10,37,NULL,6),(55,10,38,NULL,9),(56,1,37,NULL,9),(57,1,38,NULL,5);
/*!40000 ALTER TABLE `movie_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tag` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `uq_tag_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (7,'comedy'),(10,'drama'),(3,'horror'),(1,'popular'),(11,'romantic');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tv_show`
--

DROP TABLE IF EXISTS `tv_show`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tv_show` (
  `tv_show_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `serbian_title` varchar(64) NOT NULL,
  `director` varchar(64) NOT NULL,
  `synopsis` varchar(512) NOT NULL,
  `image_url` varchar(512) NOT NULL,
  `category` enum('artistic','indie','box-office','documentary','silent') NOT NULL,
  PRIMARY KEY (`tv_show_id`),
  UNIQUE KEY `uq_tv_show_title_serbian_title` (`title`,`serbian_title`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tv_show`
--

LOCK TABLES `tv_show` WRITE;
/*!40000 ALTER TABLE `tv_show` DISABLE KEYS */;
INSERT INTO `tv_show` VALUES (3,'Mr. Bean','Gospodin Bin','Richard Curtis','Bumbling, childlike Mr. Bean has trouble completing the simplest of day-to-day tasks, but his perseverance and resourcefulness frequently allow him to find ingenious ways around problems.','https://m.media-amazon.com/images/I/51bBwm0M9AL._AC_UF894,1000_QL80_.jpg','silent'),(5,'Chernobyl','Cernobil',' Craig Mazin','In April 1986, an explosion at the Chernobyl nuclear power plant in the Union of Soviet Socialist Republics becomes one of the world\'s worst man-made catastrophes.','https://m.media-amazon.com/images/I/813hrSnUOBL._AC_SL1500_.jpg','documentary'),(6,'Stranger Things','Cudne stvari','Shawn Levy','When a young boy disappears, his mother, a police chief and his friends must confront terrifying supernatural forces in order to get him back.','https://bltwebassets.azureedge.net/production/projects/stranger-things-season-1/key-art/812d23ac-61c7-4623-8ee4-58ff65618e10.png','artistic'),(10,'Bridgerton','Bridžerton','Shonda Rhimes','During the Regency era in England, eight close-knit siblings of the powerful Bridgerton family attempt to find love.','https://m.media-amazon.com/images/I/81nnLGxKFkL._AC_UF894,1000_QL80_.jpg','box-office'),(11,'Ash vs Evil Dead','Eš protiv zlih mrtvaca','Sam Raimi','Ash has spent the last thirty years avoiding responsibility, maturity, and the terrors of the Evil Dead until a Deadite plague threatens to destroy all of mankind and Ash becomes mankind\'s only hope.','https://m.media-amazon.com/images/I/91X7bNG67sL._AC_UF894,1000_QL80_.jpg','box-office'),(12,'Better Call Saul','Bolje pozovite Sola','Vince Gilligan','Ex-con artist Jimmy McGill turns into a small-time attorney and goes through a series of trials and tragedies, as he transforms into his alter ego Saul Goodman, a morally challenged criminal lawyer.','https://i.ebayimg.com/images/g/7hYAAOSwoyBibaYg/s-l1600.jpg','box-office'),(13,'Peaky Blinders','Birmingemska banda','Otto Bathurst','Tommy Shelby, a dangerous man, leads the Peaky Blinders, a gang based in Birmingham. Soon, Chester Campbell, an inspector, decides to nab him and put an end to the criminal activities.','https://cdn.shopify.com/s/files/1/0969/9128/products/PeakyBlinders-ThomasShelby-GarrisonBombing-NetflixTVShow-ArtPoster_7fef60c1-eddd-41e8-89fd-ac6edeba5038.jpg?v=1619864662','box-office'),(14,'You','Ti','John Scott','What would you do for love? For a brilliant male bookstore manager who crosses paths with an aspiring female writer, this question is put to the test. A charming yet awkward crush becomes something even more sinister when the writer becomes the manager\'s obsession. Using social media and the internet, he uses every tool at his disposal to become close to her, even going so far as to remove any obstacle --including people -- that stands in his way of getting to her.','https://m.media-amazon.com/images/I/81zXtlliBKL.jpg','box-office'),(15,'What We Do in the Shadows','Sta radimo u senci','Kyle Newacheck','Three vampires, Nandor, Colin Robinson and Nadja, struggle to keep up with the duties and responsibilities of everyday life as they reside in an apartment in New York.','https://m.media-amazon.com/images/M/MV5BNWYwNGMwNTItMzZiNS00YTNhLTg5ZDItOTE5YzdhYWRjOWQ2XkEyXkFqcGdeQXVyMTUzMTg2ODkz._V1_FMjpg_UX1000_.jpg','artistic'),(16,'The Boys','Momci','Eric Kripke','A group of vigilantes set out to take down corrupt superheroes who abuse their superpowers.','https://i.ebayimg.com/images/g/F8wAAOSw5XFhKSJF/s-l500.jpg','artistic');
/*!40000 ALTER TABLE `tv_show` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tv_show_tag`
--

DROP TABLE IF EXISTS `tv_show_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tv_show_tag` (
  `tv_show_tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `tv_show_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tv_show_tag_id`),
  KEY `fk_tv_show_tag_tag_id` (`tag_id`),
  KEY `fk_tv_show_tag_tv_show_id` (`tv_show_id`),
  CONSTRAINT `fk_tv_show_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tv_show_tag_tv_show_id` FOREIGN KEY (`tv_show_id`) REFERENCES `tv_show` (`tv_show_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tv_show_tag`
--

LOCK TABLES `tv_show_tag` WRITE;
/*!40000 ALTER TABLE `tv_show_tag` DISABLE KEYS */;
INSERT INTO `tv_show_tag` VALUES (1,1,5),(7,3,6),(9,11,10),(10,7,11),(11,3,11),(12,1,12),(13,10,12),(14,10,13),(15,10,14),(16,10,5),(17,7,3),(18,7,15),(19,3,15),(20,7,16),(21,10,16);
/*!40000 ALTER TABLE `tv_show_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tv_show_user`
--

DROP TABLE IF EXISTS `tv_show_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tv_show_user` (
  `tv_show_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `tv_show_id` int(10) unsigned NOT NULL,
  `status` enum('watched','want_to_watch','dont_want_to_watch') DEFAULT NULL,
  `rating` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`tv_show_user_id`),
  UNIQUE KEY `uq_tv_show_user_user_id_tv_show_id` (`user_id`,`tv_show_id`),
  KEY `fk_tv_show_user_tv_show_id` (`tv_show_id`),
  CONSTRAINT `fk_tv_show_user_tv_show_id` FOREIGN KEY (`tv_show_id`) REFERENCES `tv_show` (`tv_show_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tv_show_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tv_show_user`
--

LOCK TABLES `tv_show_user` WRITE;
/*!40000 ALTER TABLE `tv_show_user` DISABLE KEYS */;
INSERT INTO `tv_show_user` VALUES (1,8,3,'want_to_watch',7),(6,8,5,'watched',9),(13,8,10,'dont_want_to_watch',0),(14,8,11,NULL,4),(16,10,13,NULL,8),(17,10,14,NULL,4),(18,1,13,NULL,5),(19,1,14,NULL,8);
/*!40000 ALTER TABLE `tv_show_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password_hash` varchar(128) NOT NULL,
  `email` varchar(255) NOT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `password_reset_code` varchar(255) DEFAULT NULL,
  `rating_frequency` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uq_user_username` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'user','$2b$10$5gmjzn.vt9J67vIys.mVZ.hrZQTM6.9xvVdsx0ipq4.O0vs4M2ApS','user@gmail.com',NULL,NULL,5),(8,'test','$2b$10$vUKOW6kaXXsOjXnqm8yWU.mifYWxGT4ExPfpDfpDNCtNwXrMVXQ7K','test@test.com',NULL,NULL,60),(10,'test1','$2b$10$Nunn8AP2pfFnTPPvqCPT6OdacuCYvjl/htiUyrgJ81PW/VHDhYnvO','test1@test.com','6a3ed620-e843-47f3-8870-b8bc9226415a',NULL,12);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-26 17:52:05
