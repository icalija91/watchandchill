import * as mysql2 from "mysql2/promise";
import GenreService from "../components/genre/GenreService.service";
import MovieService from "../components/movie/MovieService.service";
import EpisodeService from "../components/tvshow/EpisodeService.service";
import AdministratorService from "../components/administrator/AdministratorService.service";
import TagService from "../components/tag/TagService.service";
import TvShowService from "../components/tvshow/TvShowService.service";
import UserService from "../components/user/UserService.service";

export interface IServices {
  genre: GenreService;
  movie: MovieService;
  administrator: AdministratorService;
  user: UserService;
  tag: TagService;
  episode: EpisodeService;
  tvShow: TvShowService;
}

export default interface IApplicationResources {
  databaseConnection: mysql2.Connection;
  services: IServices;
}
