import Ajv from "ajv";
import IServiceData from "../IServiceData.interface";

const ajv = new Ajv();

interface IRateDto {
  value: number;
}

interface IRate extends IServiceData {
  mark_value: string;
}

const RateValidator = ajv.compile({
  type: "object",
  properties: {
    value: {
      type: "integer",
      minimum: 1,
      maximum: 10,
    },
  },
  required: ["value"],
  additionalProperties: false,
});

export { IRateDto, IRate, RateValidator };
