import Ajv from "ajv";
import IServiceData from "../IServiceData.interface";

const ajv = new Ajv();

interface IStatusDto {
  value: string;
}

interface IStatus extends IServiceData {
  mark_value: string;
}

const StatusValidator = ajv.compile({
  type: "object",
  properties: {
    value: {
      enum: ["watched", "want_to_watch", "dont_want_to_watch"],
    },
  },
  required: ["value"],
  additionalProperties: false,
});

export { IStatusDto, IStatus, StatusValidator };
