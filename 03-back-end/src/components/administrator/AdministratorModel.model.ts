import IModel from "../../common/IModelInterface.interface";

export default class AdministratorModel implements IModel {
  administratorId: number;
  username: string;
  passwordHash?: string;
}
