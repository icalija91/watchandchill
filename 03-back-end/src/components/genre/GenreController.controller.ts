import { Request, Response } from 'express';
import { AddGenreValidator } from './dto/IAddGenre.dto';
import GenreService, { DefaultGenreAdapterOptions } from './GenreService.service';
import IAddGenre from './dto/IAddGenre.dto';
import IEditGenre, { EditGenreValidator } from './dto/IEditGenre.dto';
import BaseController from '../../common/BaseController';

class GenreController extends BaseController {

    async getAll(req: Request, res: Response){

        this.services.genre.getAll(DefaultGenreAdapterOptions)
        .then(result => {
            res.send(result);
        })
        .catch(error => {
            res.status(500).send(error?.message);
        });

    }

    async getById(req: Request, res: Response){
        const id: number = +req.params?.id;

        this.services.genre.getById(id, DefaultGenreAdapterOptions)
        .then(result =>{

            if (result === null){

                return res.sendStatus(404);
            }

            res.send(result);
        })
        .catch(error => {
            res.status(500).send(error?.message);
        });
    }

    async add(req: Request, res: Response) {

        const data = req.body as IAddGenre;

        if ( !AddGenreValidator(data) ) {
            return res.status(400).send(AddGenreValidator.errors);
        }

        this.services.genre.add(data, DefaultGenreAdapterOptions)
            .then(result => {
                res.send(result);
            })
            .catch(error => {
                res.status(400).send(error?.message);
            });
    }

    async edit(req: Request, res: Response){
        const id: number = +req.params?.id;
        const data = req.body as IEditGenre;

        if ( !EditGenreValidator(data) ) {
            return res.status(400).send(EditGenreValidator.errors);
        }

        this.services.genre.getById(id, DefaultGenreAdapterOptions)
        .then(result =>{

            if (result === null){

                return res.sendStatus(404);
            }

            this.services.genre.editById(id, {
                name: data.name
            }, DefaultGenreAdapterOptions)
            .then(result => {
                res.send(result);
            })
            .catch(error => {
                res.status(400).send(error?.message);
            })
        })
        .catch(error => {
            res.status(500).send(error?.message);
        });
    }

    async delete(req: Request, res: Response){
        const id: number = +req.params?.id;

        this.services.genre.getById(id, DefaultGenreAdapterOptions)
        .then(result =>{

            if (result === null){

                return res.sendStatus(404);
            }

            this.services.genre.deleteById(id)
            .then(result => {
                res.send("This genre has been deleted");
            })
            .catch(error => {
                res.status(400).send(error?.message);
            })
        })
        .catch(error => {
            res.status(500).send(error?.message);
        });
    }
}

export default GenreController;