import IModel from "../../common/IModelInterface.interface";
import MovieModel from "../movie/MovieModel.model";
class GenreModel implements IModel {
  genreId: number;
  name: string;
}

export default GenreModel;
