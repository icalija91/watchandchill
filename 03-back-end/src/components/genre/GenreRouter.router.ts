import GenreController from "./GenreController.controller";
import GenreService from "./GenreService.service";
import * as express from "express";
import IApplicationResources from '../../common/IApplicationResources.interface';
import IRouter from '../../common/IRouter.interface';
import AuthMiddleware from "../../middlewares/AuthMiddleware";

class GenreRouter implements IRouter{

    public setupRoutes(application: express.Application, resources: IApplicationResources){
        
        const genreController: GenreController = new GenreController(resources.services);

        application.get("/api/genre",AuthMiddleware.getVerifier("administrator","user"), genreController.getAll.bind(genreController));
        application.get("/api/genre/:id", AuthMiddleware.getVerifier("administrator", "user"), genreController.getById.bind(genreController));
        application.post("/api/genre", AuthMiddleware.getVerifier("administrator"), genreController.add.bind(genreController));
        application.put("/api/genre/:id",AuthMiddleware.getVerifier("administrator"), genreController.edit.bind(genreController));
        application.delete("/api/genre/:id", AuthMiddleware.getVerifier("administrator"), genreController.delete.bind(genreController));
    }
}

export default GenreRouter;