import GenreModel from './GenreModel.model';
import IAddGenre from './dto/IAddGenre.dto';
import BaseService from '../../common/BaseService';
import IEditGenre from './dto/IEditGenre.dto';
import IAdapterOptions from '../../common/IAdapterOptions.interface';

interface GenreAdapterOptions extends IAdapterOptions{


}

const DefaultGenreAdapterOptions: GenreAdapterOptions = {

}

interface MovieGenreInterface {
    movie_genre_id: number,
    movie_id: number,
    genre_id: number,
}


class GenreService extends BaseService<GenreModel, GenreAdapterOptions>{

    tableName(): string {
        return "genre";
    }

    protected async adaptToModel(data: any, options: GenreAdapterOptions): Promise<GenreModel>{
        
        return new Promise(resolve => {
            const genre: GenreModel = new GenreModel();
    
            genre.genreId = +data?.genre_id;
            genre.name = data?.name;
            resolve(genre);
        })
    }

   

    public async add(data: IAddGenre, options: GenreAdapterOptions): Promise<GenreModel>{
        return this.baseAdd(data, options);
    }

    public async editById(genreId: number, data: IEditGenre, options: GenreAdapterOptions): Promise <GenreModel>{
        return this.baseEditById(genreId, data, options);
    }

    public async deleteById(genreId: number): Promise <true>{
        return this.baseDeleteById(genreId);
    }

    public async getAllByMovieId(movieId: number, options: GenreAdapterOptions = {}): Promise<GenreModel[]> {
        return new Promise((resolve, reject) => {
            this.getAllFromTableByFieldNameAndValue<MovieGenreInterface>("movie_genre", "movie_id", movieId)
            .then(async result => {
                const genreIds = result.map(ii => ii.genre_id);

                const genres: GenreModel[] = [];

                for (let genreId of genreIds) {
                    const genre = await this.getById(genreId, options);
                    genres.push(genre);
                }

                resolve(genres);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
}

export default GenreService;
export { DefaultGenreAdapterOptions };