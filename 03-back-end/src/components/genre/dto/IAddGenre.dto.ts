import Ajv from "ajv";
import IServiceData from "../../../common/IServiceData.interface";

const ajv = new Ajv();

export default interface IAddGenre extends IServiceData {
  name: string;
}

const AddGenreSchema = {
  type: "object",
  properties: {
    name: {
      type: "string",
      minLength: 4,
      maxLength: 128,
    },
  },
  required: ["name"],
  additionalProperties: false,
};

const AddGenreValidator = ajv.compile(AddGenreSchema);

export { AddGenreValidator };
