import Ajv from "ajv";
import IServiceData from "../../../common/IServiceData.interface";

const ajv = new Ajv();

export default interface IEditGenre extends IServiceData {
  name: string;
}

const EditGenreSchema = {
  type: "object",
  properties: {
    name: {
      type: "string",
      minLength: 4,
      maxLength: 128,
    },
  },
  required: ["name"],
  additionalProperties: false,
};

const EditGenreValidator = ajv.compile(EditGenreSchema);

export { EditGenreValidator };
