import { Request, Response } from "express";
import BaseController from "../../common/BaseController";
import IAddMovie, {
  AddMovieValidator,
  IAddMovieServiceDto,
} from "./dto/IAddMovie.dto";
import { DefaultMovieAdapterOptions } from "./MovieService.service";
import { EditMovieValidator, IEditMovieDto } from "./dto/IEditMovie.dto";
import { DefaultGenreAdapterOptions } from "../genre/GenreService.service";
import { DefaultTagAdapterOptions } from "../tag/TagService.service";
import { IRateDto, RateValidator } from "../../common/dto/IRate.dto";
import { IStatusDto, StatusValidator } from "../../common/dto/IStatus.dto";

class MovieController extends BaseController {
  async getAll(req: Request, res: Response) {
    this.services.movie
      .getAll(DefaultMovieAdapterOptions)
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async getRecommended(req: Request, res: Response) {
    const userId: number = req.authorisation?.id;
    this.services.movie
      .getRecommended(userId)
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async getById(req: Request, res: Response) {
    const id: number = +req.params?.id;

    this.services.movie
      .getById(id, DefaultMovieAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.sendStatus(404);
        }

        res.send(result);
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async add(req: Request, res: Response) {
    const data = req.body as IAddMovieServiceDto;

    if (!AddMovieValidator(data)) {
      return res.status(400).send(AddMovieValidator.errors);
    }

    this.services.movie
      .add({
        title: data.title,
        serbianTitle: data.serbianTitle,
        director: data.director,
        synopsis: data.synopsis,
        imageUrl: data.imageUrl,
        category: data.category,
        genreIds: [],
        tagIds: [],
      })
      .then((newMovie) => {
        for (let givenTagId of data.tagIds) {
          this.services.movie
            .addMovieTag({
              movie_id: newMovie.movieId,
              tag_id: givenTagId,
            })
            .catch((error) => {
              throw {
                status: 500,
                message: error?.message,
              };
            });
        }

        return newMovie;
      })
      .then((newMovie) => {
        for (let givenGenreId of data.genreIds) {
          this.services.movie
            .addMovieGenre({
              movie_id: newMovie.movieId,
              genre_id: givenGenreId,
            })
            .catch((error) => {
              throw {
                status: 500,
                message: error?.message,
              };
            });
        }

        return newMovie;
      })
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(400).send(error?.message);
      });
  }

  async edit(req: Request, res: Response) {
    const movieId: number = +req.params?.id;
    const data = req.body as IEditMovieDto;

    if (!EditMovieValidator(data)) {
      return res.status(400).send(EditMovieValidator.errors);
    }

    this.services.movie
      .edit(
        movieId,
        {
          title: data.title,
          serbianTitle: data.serbianTitle,
          director: data.director,
          synopsis: data.synopsis,
          imageUrl: data.imageUrl,
          category: data.category,
          genreIds: [],
          tagIds: [],
        },
        {
          loadGenres: true,
          loadTags: true,
        }
      )
      .then(async (result) => {
        const currentGenreId = result.genreIds?.map((genre) => genre.genreId);
        const newGenreIds = data.genreIds;

        const availableGenres = await this.services.genre.getAllIds(
          "genre_id",
          DefaultGenreAdapterOptions
        );
        const availableGenreIds = availableGenres.map((genre) => genre.genreId);
        for (let id of newGenreIds) {
          if (!availableGenreIds.includes(id)) {
            throw {
              status: 400,
              message: "Genre " + id + " is not available!",
            };
          }
        }

        const genreIdsToAdd = newGenreIds.filter(
          (id) => !currentGenreId.includes(id)
        );
        for (let id of genreIdsToAdd) {
          if (
            !(await this.services.movie.addMovieGenre({
              movie_id: result.movieId,
              genre_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error adding a new genre to this movie!",
            };
          }
        }

        const movieIdsToDelete = currentGenreId.filter(
          (id) => !newGenreIds.includes(id)
        );
        for (let id of movieIdsToDelete) {
          if (
            !(await this.services.movie.deleteMovieGenre({
              movie_id: result.movieId,
              genre_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error deleting an existing genre from this movie!",
            };
          }
        }

        return result;
      })
      .then(async (result) => {
        const currentTagId = result.tagIds?.map((tag) => tag.tagId);
        const newTagIds = data.tagIds;

        const availableTags = await this.services.tag.getAllIds(
          "tag_id",
          DefaultTagAdapterOptions
        );
        const availableTagIds = availableTags.map((tag) => tag.tagId);
        for (let id of newTagIds) {
          if (!availableTagIds.includes(id)) {
            throw {
              status: 400,
              message: "Tag " + id + " is not available!",
            };
          }
        }

        const tagIdsToAdd = newTagIds.filter(
          (id) => !currentTagId.includes(id)
        );
        for (let id of tagIdsToAdd) {
          if (
            !(await this.services.movie.addMovieTag({
              movie_id: result.movieId,
              tag_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error adding a new tag to this movie!",
            };
          }
        }

        const movieIdsToDelete = currentTagId.filter(
          (id) => !newTagIds.includes(id)
        );
        for (let id of movieIdsToDelete) {
          if (
            !(await this.services.movie.deleteMovieTag({
              movie_id: result.movieId,
              tag_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error deleting an existing tag from this movie!",
            };
          }
        }

        return result;
      })
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(400).send(error?.message);
      });
  }

  async delete(req: Request, res: Response) {
    const movieId: number = +req.params?.id;

    this.services.movie
      .getById(movieId, DefaultMovieAdapterOptions)
      .then((result) => {
        if (result === null) throw { status: 404, message: "Movie not found!" };
        return result;
      })
      .then(async (result) => {
        const currentTagId = result.tagIds?.map((tag) => tag.tagId);

        for (let id of currentTagId) {
          if (
            !(await this.services.movie.deleteMovieTag({
              movie_id: result.movieId,
              tag_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error deleting an existing tag from this movie!",
            };
          }
        }

        return result;
      })
      .then(async (result) => {
        const currentGenreId = result.genreIds?.map((genre) => genre.genreId);

        for (let id of currentGenreId) {
          if (
            !(await this.services.movie.deleteMovieGenre({
              movie_id: result.movieId,
              genre_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error deleting an existing genre from this movie!",
            };
          }
        }

        return result;
      })
      .then((movie) => {
        return this.services.movie.deleteById(movie.movieId);
      })
      .then(() => {
        res.send("Deleted!");
      })
      .catch((error) => {
        res
          .status(error?.status ?? 500)
          .send(error?.message ?? "Server side error!");
      });
  }

  async getRatingByMovieIdAndUserId(req: Request, res: Response) {
    const movieId: number = +req.params?.id;
    const userId: number = req.authorisation?.id;

    this.services.movie
      .getRatingByMovieIdAndUserId(movieId, userId, DefaultMovieAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.sendStatus(404);
        }
        res.send(String(result));
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  public async rate(req: Request, res: Response) {
    const movieId = +req.params?.id;
    const userId = req.authorisation?.id;
    const data = req.body as IRateDto;

    if (!RateValidator(data)) {
      return res.status(400).send(RateValidator.errors);
    }

    this.services.movie
      .rate(movieId, userId, {
        mark_value: "" + data.value,
      })
      .then(() => {
        this.services.user.updateRatingFrequency(userId);
      })
      .then(() => {
        res.send("Rating saved");
      })
      .catch((error) => {
        res.status(error?.status ?? 500).send(error?.message);
      });
  }

  public async status(req: Request, res: Response) {
    const movieId = +req.params?.id;
    const userId = req.authorisation?.id;
    const data = req.body as IStatusDto;

    if (!StatusValidator(data)) {
      return res.status(400).send(StatusValidator.errors);
    }

    this.services.movie
      .status(movieId, userId, {
        mark_value: "" + data.value,
      })
      .then(() => {
        res.send("Status saved");
      })
      .catch((error) => {
        res.status(error?.status ?? 500).send(error?.message);
      });
  }

  async getStatusByMovieId(req: Request, res: Response) {
    const movieId: number = +req.params?.id;
    const userId: number = req.authorisation?.id;

    this.services.movie
      .getStatusByMovieId(movieId, userId, DefaultMovieAdapterOptions)
      .then((result) => {
        res.send(String(result));
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }
}

export default MovieController;
