
import * as express from "express";
import IApplicationResources from '../../common/IApplicationResources.interface';
import IRouter from '../../common/IRouter.interface';
import MovieController from './MovieController.controller';
import AuthMiddleware from "../../middlewares/AuthMiddleware";

class MovieRouter implements IRouter{

    public setupRoutes(application: express.Application, resources: IApplicationResources){
        
        const movieController: MovieController = new MovieController(resources.services);

        application.get("/api/movie", AuthMiddleware.getVerifier("administrator", "user"), movieController.getAll.bind(movieController));
        application.get("/api/movie/recommended", AuthMiddleware.getVerifier("user"), movieController.getRecommended.bind(movieController));
        application.get("/api/movie/:id", AuthMiddleware.getVerifier("administrator", "user"), movieController.getById.bind(movieController));
        application.post("/api/movie", AuthMiddleware.getVerifier("administrator"), movieController.add.bind(movieController));
        application.put("/api/movie/:id",AuthMiddleware.getVerifier("administrator"),  movieController.edit.bind(movieController));
        application.delete("/api/movie/:id", AuthMiddleware.getVerifier("administrator"),movieController.delete.bind(movieController));
        application.post("/api/movie/:id/rate",           AuthMiddleware.getVerifier("user"), movieController.rate.bind(movieController));
        application.get("/api/movie/:id/rate",           AuthMiddleware.getVerifier("user"), movieController.getRatingByMovieIdAndUserId.bind(movieController));
        application.post("/api/movie/:id/status",           AuthMiddleware.getVerifier("user"), movieController.status.bind(movieController));
        application.get("/api/movie/:id/status",           AuthMiddleware.getVerifier("user"), movieController.getStatusByMovieId.bind(movieController));

    }
}

export default MovieRouter;