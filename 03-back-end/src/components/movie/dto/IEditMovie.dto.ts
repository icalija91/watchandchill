import Ajv from "ajv";
import IServiceData from "../../../common/IServiceData.interface";

const ajv = new Ajv();

interface IEditMovieDto {
  title: string;
  serbianTitle: string;
  director: string;
  synopsis: string;
  imageUrl: string;
  category: Enumerator;
  genreIds: number[];
  tagIds: number[];
}

const EditMovieSchema = {
  type: "object",
  properties: {
    title: {
      type: "string",
      minLength: 1,
      maxLength: 128,
    },
    serbianTitle: {
      type: "string",
      minLength: 1,
      maxLength: 128,
    },
    director: {
      type: "string",
      minLength: 2,
      maxLength: 32,
    },
    synopsis: {
      type: "string",
      minLength: 4,
      maxLength: 512,
    },
    imageUrl: {
      type: "string",
      minLength: 4,
      maxLength: 512,
    },
    category: {
      type: "string",
      enum: ["artistic", "indie", "box-office", "documentary", "silent"],
    },
    genreIds: {
      type: "array",
      minItems: 1,
      uniqueItems: true,
      items: { type: "integer" },
    },
    tagIds: {
      type: "array",
      minItems: 0,
      uniqueItems: true,
      items: { type: "integer" },
    },
  },
  required: [
    "title",
    "genreIds",
    "serbianTitle",
    "director",
    "synopsis",
    "imageUrl",
    "category",
  ],
  additionalProperties: false,
};

const EditMovieValidator = ajv.compile(EditMovieSchema);

export { EditMovieValidator, IEditMovieDto };
