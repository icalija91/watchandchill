import IModel from "../../common/IModelInterface.interface";
import GenreModel from "../genre/GenreModel.model";
import TagModel from "../tag/TagModel.model";
class MovieModel implements IModel {
  movieId: number;
  title: string;
  serbianTitle: string;
  director: string;
  synopsis: string;
  imageUrl: string;
  category: "artistic" | "indie" | "box-office" | "documentary" | "silent";

  genreIds: GenreModel[] = [];
  tagIds?: TagModel[] = [];
}

export default MovieModel;
