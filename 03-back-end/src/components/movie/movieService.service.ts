import MovieModel from "./MovieModel.model";
import * as mysql2 from "mysql2/promise";
import BaseService from "../../common/BaseService";
import IAdapterOptions from "../../common/IAdapterOptions.interface";
import {
  IAddMovieServiceDto,
  IMovieGenre,
  IMovieTag,
} from "./dto/IAddMovie.dto";
import { IEditMovieDto } from "./dto/IEditMovie.dto";
import { IRate, IRateDto } from "../../common/dto/IRate.dto";
import { IStatus } from "../../common/dto/IStatus.dto";
import { MovieTagInterface } from "../tag/TagService.service";
import TagModel from "../tag/TagModel.model";
import { resolve } from "path";
import ITag from "../../../../04-front-end/src/models/ITag.model";

export interface IMovieAdapterOptions extends IAdapterOptions {
  loadTags: boolean;
  loadGenres: boolean;
}

export const DefaultMovieAdapterOptions: IMovieAdapterOptions = {
  loadTags: true,
  loadGenres: true,
};

interface MovieUserInterface {
  movie_user_id: number;
  movie_id: number;
  user_id: number;
  rating: number;
  status: Enumerator;
}

export interface IMovieWeights {
  movieId: number;
  weight: number;
}

export default class IMovieService extends BaseService<
  MovieModel,
  IMovieAdapterOptions
> {
  tableName(): string {
    return "movie";
  }

  protected async adaptToModel(
    data: any,
    options: IMovieAdapterOptions
  ): Promise<MovieModel> {
    return new Promise(async (resolve) => {
      const movie: MovieModel = new MovieModel();

      movie.movieId = +data?.movie_id;
      movie.title = data?.title;
      movie.serbianTitle = data?.serbian_title;
      movie.director = data?.director;
      movie.synopsis = data?.synopsis;
      movie.imageUrl = data?.image_url;
      movie.category = data?.category;

      if (options.loadGenres) {
        movie.genreIds = await this.services.genre.getAllByMovieId(
          movie.movieId,
          {}
        );
      }

      if (options.loadTags) {
        movie.tagIds = await this.services.tag.getAllByMovieId(
          movie.movieId,
          {}
        );
      }
      resolve(movie);
    });
  }

  public async getRecommended(userId: number): Promise<MovieModel[]> {
    let recommendedMovies = [];
    let filteredMovies = await this.getMoviesBasedOnUserPreference(userId);
    //old user with movie preferene
    if (filteredMovies.length !== 0) {
      let weights = await this.calculateWeights(filteredMovies);
      recommendedMovies = await this.getRecommendedMovies(
        filteredMovies,
        weights
      );
    } else {
      //new user with no movie preferene, return the default list
      let allMovies = await this.getAll(DefaultMovieAdapterOptions);
      recommendedMovies = allMovies.slice(0, 10);
    }
    return recommendedMovies;
  }

  public async getRecommendedMovies(
    filteredMovies: number[],
    weights: number[]
  ): Promise<MovieModel[]> {
    let movieWeights = [];
    //Create a list containing movieId/weight objects
    for (let i = 0; i < filteredMovies.length; i++) {
      let movieWeight: IMovieWeights = {
        movieId: filteredMovies[i],
        weight: weights[i],
      };
      movieWeights.push(movieWeight);
    }
    // Sort the array based on weights by descending order
    movieWeights.sort((a, b) => b.weight - a.weight);
    //get top 10
    const topTenMovieArray = movieWeights.slice(0, 10);

    let promiseMovies = [];
    topTenMovieArray.forEach((movieWeight) => {
      promiseMovies.push(
        this.services.movie.getById(
          movieWeight.movieId,
          DefaultMovieAdapterOptions
        )
      );
    });
    let movies = await Promise.all(promiseMovies);

    return movies;
  }

  public async calculateWeights(filteredMovies: number[]): Promise<number[]> {
    const ratingsAndUsers = await Promise.all(
      filteredMovies.map((movieId) =>
        this.services.movie.getRatingByMovieId(
          movieId,
          DefaultMovieAdapterOptions
        )
      )
    );

    const weights = [];
    for (const ratingAndUser of ratingsAndUsers) {
      const ratingFrequency = await Promise.all(
        ratingAndUser.map((m) =>
          this.services.user.getRatingFrequency(m.user_id)
        )
      );
      let rating = ratingAndUser.map((m) => m.rating);

      let movieWeights = this.weightedMean(rating, ratingFrequency);
      weights.push(movieWeights);
    }
    return weights;
  }

  public weightedMean(ratings, ratingFrequency) {
    // Calculate the weighted mean
    let sum = 0;
    let totalVotes = 0;
    let weightedMean = [];

    for (let i = 0; i < ratings.length; i++) {
      const weightedRating = ratings[i] * ratingFrequency[i];
      sum += weightedRating;
      totalVotes += ratingFrequency[i];
    }
    weightedMean.push(sum / totalVotes);
    return weightedMean;
  }

  public async getMoviesBasedOnUserPreference(
    userId: number
  ): Promise<number[]> {
    let tagsToFilterOut =
      await this.getAllTagsWhereStatusDontWantToWatchOrLowerRating(userId);
    let tagsToFilterIn = await this.getAllTagsWhereStatusWatchedAndHighRating(
      userId
    );
    let filteredMovies = await this.getAllMoviesBasedOnTags(
      tagsToFilterIn,
      tagsToFilterOut,
      userId
    );
    return filteredMovies;
  }

  public async getAllMoviesBasedOnTags(
    tagsToFilterIn: TagModel[],
    tagsToFilterOut: TagModel[],
    userId: number
  ): Promise<number[]> {
    let positiveTagIds = tagsToFilterIn.map((tag) => tag.tagId);
    let negativeTagIds = tagsToFilterOut.map((tag) => tag.tagId);
    let positiveMovies = [];
    let negativeMovies = [];
    if (positiveTagIds.length !== 0) {
       let positiveArray = await this.getAllMovieIdsBasedOnTags(positiveTagIds);
       //filter out duplicates
       positiveMovies = Array.from(new Set(positiveArray));
    }
    if (negativeTagIds.length !== 0) {
      let negativeArray = await this.getAllMovieIdsBasedOnTags(negativeTagIds);
      negativeMovies = Array.from(new Set(negativeArray));
    }
    let userMovies = await this.getAllMoviesByUserId(userId);

    let moviesToFilterOut = negativeMovies.concat(userMovies);
    //return only movies that user didnt rate/set status and which dont include negative tags
    return positiveMovies.filter((el) => !moviesToFilterOut.includes(el));
  }

  public async getAllMoviesByUserId(userId: number): Promise<number[]> {
    return new Promise((resolve, reject) => {
      this.getAllFromTableByFieldNameAndValue<MovieUserInterface>(
        "movie_user",
        "user_id",
        userId
      )
        .then((result) => {
          const movieIds = result.map((ii) => ii.movie_id);
          resolve(movieIds);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getAllMovieIdsBasedOnTags(
    positiveTagIds: any
  ): Promise<number[]> {
    return new Promise((resolve, reject) => {
      this.getAllFromTableByFieldValuesArray<MovieTagInterface>(
        "movie_tag",
        positiveTagIds
      )
        .then((result) => {
          const movieIds = result.map((ii) => ii.movie_id);
          resolve(movieIds);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getAllTagsWhereStatusDontWantToWatchOrLowerRating(
    userId: number
  ): Promise<TagModel[]> {
    let tagsLowerRating = await this.getAllTagsWithLowRating(userId);
    let tagsDontWantToWatch = await this.getAllTagsWhereStatusDontWantToWatch(
      userId
    );
    return [...tagsLowerRating, ...tagsDontWantToWatch].flat();
  }

  public async getAllTagsWhereStatusWatchedAndHighRating(
    userId: number
  ): Promise<TagModel[]> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValuesWhereRatingHigh<MovieUserInterface>(
        "movie_user",
        "status",
        "watched",
        "user_id",
        userId,
        DefaultMovieAdapterOptions
      )
        .then(async (result) => {
          const movieIds = result.map((ii) => ii.movie_id);
          if (movieIds[0] !== undefined) {
            const filteredTagIds = await Promise.all(
              movieIds.map((movieId) =>
                this.services.tag.getAllByMovieId(movieId, {})
              )
            );
            resolve(filteredTagIds.flat());
          } else {
            resolve([]);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getAllTagsWhereStatusDontWantToWatch(
    userId: number
  ): Promise<TagModel[]> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValues<MovieUserInterface>(
        "movie_user",
        "status",
        "dont_want_to_watch",
        "user_id",
        userId,
        DefaultMovieAdapterOptions
      )
        .then(async (result) => {
          const movieIds = result.map((ii) => ii.movie_id);
          if (movieIds[0] !== undefined) {
            const filteredTagIds = await Promise.all(
              movieIds.map((movieId) =>
                this.services.tag.getAllByMovieId(movieId, {})
              )
            );
            resolve(filteredTagIds.flat());
          } else {
            resolve([]);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getAllTagsWithLowRating(userId: number): Promise<TagModel[]> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValuesLessOrEqual<MovieUserInterface>(
        "movie_user",
        "rating",
        5,
        "user_id",
        userId,
        DefaultMovieAdapterOptions
      )
        .then(async (result) => {
          const movieIds = result.map((ii) => ii.movie_id);
          if (movieIds[0] !== undefined) {
            const filteredTagIds = await Promise.all(
              movieIds.map((movieId) =>
                this.services.tag.getAllByMovieId(movieId, {})
              )
            );
            resolve(filteredTagIds.flat());
          } else {
            resolve([]);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async add(data: IAddMovieServiceDto): Promise<MovieModel> {
    return new Promise<MovieModel>((resolve, reject) => {
      const sql: string =
        "INSERT `movie` SET `title` =?, `serbian_title` =?, `director` =?, `synopsis` =?, `image_url` =?, `category` =?;";

      this.db
        .execute(sql, [
          data.title,
          data.serbianTitle,
          data.director,
          data.synopsis,
          data.imageUrl,
          data.category,
        ])
        .then(async (result) => {
          const info: any = result;

          const newMovieId = +info[0]?.insertId;

          const newMovie: MovieModel | null = await this.getById(newMovieId, {
            loadTags: true,
            loadGenres: true,
          });

          if (newMovie === null) {
            return reject({ message: "Duplicate movie title in this genre!" });
          }

          resolve(newMovie);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async addMovieTag(data: IMovieTag): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string = "INSERT movie_tag SET movie_id = ?, tag_id = ?;";

      this.db
        .execute(sql, [data.movie_id, data.tag_id])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async addMovieGenre(data: IMovieGenre): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string = "INSERT movie_genre SET movie_id = ?, genre_id = ?;";

      this.db
        .execute(sql, [data.movie_id, data.genre_id])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async rate(movieId: number, userId: number, data: IRate): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "INSERT movie_user SET movie_id = ?, user_id = ?, rating = ? ON DUPLICATE KEY UPDATE rating = VALUES(rating);";

      this.db
        .execute(sql, [movieId, userId, data.mark_value])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getRatingByMovieId(
    movieId: number,
    options: IMovieAdapterOptions
  ): Promise<MovieUserInterface[]> {
    return new Promise((resolve, reject) => {
      this.getAllFromTableByFieldNameAndValue<MovieUserInterface>(
        "movie_user",
        "movie_id",
        movieId
      )
        .then(async (result) => {
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getRatingByMovieIdAndUserId(
    movieId: number,
    userId: number,
    options: IMovieAdapterOptions
  ): Promise<number> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValues<MovieUserInterface>(
        "movie_user",
        "movie_id",
        movieId,
        "user_id",
        userId,
        options
      )
        .then(async (result) => {
          const ratings = result.map((ii) => ii.rating);
          if (ratings[0] !== undefined) {
            resolve(ratings[0]);
          }
          //user did not rate
          resolve(0);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async status(
    movieId: number,
    userId: number,
    data: IStatus
  ): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "INSERT movie_user SET movie_id = ?, user_id = ?, status = ? ON DUPLICATE KEY UPDATE status = VALUES(status);";

      this.db
        .execute(sql, [movieId, userId, data.mark_value])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getStatusByMovieId(
    movieId: number,
    userId: number,
    options: IMovieAdapterOptions
  ): Promise<Enumerator> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValues<MovieUserInterface>(
        "movie_user",
        "movie_id",
        movieId,
        "user_id",
        userId,
        options
      )
        .then(async (result) => {
          const statuses = result.map((ii) => ii.status);
          if (statuses[0] !== undefined) {
            resolve(statuses[0]);
          }
          //user did not set status
          resolve(null);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async edit(
    movieId: number,
    data: IEditMovieDto,
    options: IMovieAdapterOptions
  ): Promise<MovieModel> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "UPDATE `movie` SET `title` =?, `serbian_title` =?, `director` =?, `synopsis` =?, `image_url` =?, `category` =? WHERE `movie_id` =?;";
      this.db
        .execute(sql, [
          data.title,
          data.serbianTitle,
          data.director,
          data.synopsis,
          data.imageUrl,
          data.category,
          movieId,
        ])
        .then(async (result) => {
          const info: any = result;

          if (info[0]?.affectedRows === 0) {
            return reject({ message: "Could not edit movie!" });
          }

          const movie: MovieModel | null = await this.getById(movieId, options);

          if (movie === null) {
            return reject({ message: "Could not find this movie!" });
          }

          resolve(movie);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async deleteMovieGenre(data: IMovieGenre): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "DELETE FROM movie_genre WHERE movie_id = ? AND genre_id = ?;";

      this.db
        .execute(sql, [data.movie_id, data.genre_id])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.affectedRows);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async deleteMovieTag(data: IMovieTag): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "DELETE FROM movie_tag WHERE movie_id = ? AND tag_id = ?;";

      this.db
        .execute(sql, [data.movie_id, data.tag_id])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.affectedRows);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async deleteById(movieId: number): Promise<true> {
    return this.baseDeleteById(movieId);
  }
}
