import { Request, Response } from "express";
import BaseController from "../../common/BaseController";

import * as bcrypt from "bcrypt";
import IAddTag, { AddTagValidator } from "./dto/IAddTag.dto";
import IEditTag, { EditTagValidator } from "./dto/IEditTag.dto";
import { DefaultTagAdapterOptions } from "./TagService.service";

export default class TagController extends BaseController {
  async getAll(req: Request, res: Response) {
    this.services.tag
      .getAll(DefaultTagAdapterOptions)
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async getById(req: Request, res: Response) {
    const id: number = +req.params?.id;

    this.services.tag
      .getById(id, DefaultTagAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.sendStatus(404);
        }

        res.send(result);
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async add(req: Request, res: Response) {
    const data = req.body as IAddTag;

    if (!AddTagValidator(data)) {
      return res.status(400).send(AddTagValidator.errors);
    }

    this.services.tag
      .add(data, DefaultTagAdapterOptions)
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(400).send(error?.message);
      });
  }

  async edit(req: Request, res: Response) {
    const id: number = +req.params?.id;
    const data = req.body as IEditTag;

    if (!EditTagValidator(data)) {
      return res.status(400).send(EditTagValidator.errors);
    }

    this.services.tag
      .getById(id, DefaultTagAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.sendStatus(404);
        }

        this.services.tag
          .editById(
            id,
            {
              name: data.name,
            },
            DefaultTagAdapterOptions
          )
          .then((result) => {
            res.send(result);
          })
          .catch((error) => {
            res.status(400).send(error?.message);
          });
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async delete(req: Request, res: Response) {
    const id: number = +req.params?.id;

    this.services.tag
      .getById(id, DefaultTagAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.sendStatus(404);
        }

        this.services.tag
          .deleteById(id)
          .then((result) => {
            res.send("This tag has been deleted");
          })
          .catch((error) => {
            res.status(400).send(error?.message);
          });
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }
}
