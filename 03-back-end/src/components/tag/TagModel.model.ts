import IModel from "../../common/IModelInterface.interface";
class TagModel implements IModel {
  tagId: number;
  name: string;
}

export default TagModel;
