import * as express from "express";
import IApplicationResources from '../../common/IApplicationResources.interface';
import IRouter from '../../common/IRouter.interface';
import TagController from "./TagController.coontroller";
import AuthMiddleware from "../../middlewares/AuthMiddleware";

class TagRouter implements IRouter{

    public setupRoutes(application: express.Application, resources: IApplicationResources){
        
        const tagController: TagController = new TagController(resources.services);

        application.get("/api/tag", AuthMiddleware.getVerifier("administrator","user"), tagController.getAll.bind(tagController));
        application.get("/api/tag/:id", AuthMiddleware.getVerifier("administrator","user"), tagController.getById.bind(tagController));
        application.post("/api/tag", AuthMiddleware.getVerifier("administrator"), tagController.add.bind(tagController));
        application.put("/api/tag/:id", AuthMiddleware.getVerifier("administrator"), tagController.edit.bind(tagController));
        application.delete("/api/tag/:id",AuthMiddleware.getVerifier("administrator"), tagController.delete.bind(tagController));
    }
}

export default TagRouter;