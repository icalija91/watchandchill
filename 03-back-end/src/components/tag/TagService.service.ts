import * as mysql2 from 'mysql2/promise';
import BaseService from '../../common/BaseService';
import IAdapterOptions from '../../common/IAdapterOptions.interface';
import TagModel from './TagModel.model';
import IAddTag from './dto/IAddTag.dto';
import IEditTag from './dto/IEditTag.dto';

export interface TagAdapterOptions  extends IAdapterOptions{

}

export const DefaultTagAdapterOptions: TagAdapterOptions = {

}

export interface MovieTagInterface {
    movie_tag_id: number,
    movie_id: number,
    tag_id: number,
}

export interface EpisodeTagInterface {
    episode_tag_id: number,
    episode_id: number,
    tag_id: number,
}

export interface TvShowTagInterface {
    tv_show_tag_id: number,
    tv_show_id: number,
    tag_id: number,
}

export default class TagService extends BaseService<TagModel, TagAdapterOptions>{

    tableName(): string {
        return "tag";
    }

    protected async adaptToModel(data: any, options:  TagAdapterOptions): Promise<TagModel>{
        return new Promise(resolve => {
        const tag: TagModel = new TagModel();

        tag.tagId = +data?.tag_id;
        tag.name = data?.name;
        resolve(tag);
    })
    }

   

    public async add(data: IAddTag, options: TagAdapterOptions): Promise<TagModel>{
        return this.baseAdd(data, options);
    }

    public async deleteById(tagId: number): Promise <true>{
        return this.baseDeleteById(tagId);
    }

    public async editById(tagId: number, data: IEditTag, options: TagAdapterOptions): Promise <TagModel>{
        return this.baseEditById(tagId, data, options);
    }
    

    public async getAllByMovieId(movieId: number, options: TagAdapterOptions = {}): Promise<TagModel[]> {
        return new Promise((resolve, reject) => {
            this.getAllFromTableByFieldNameAndValue<MovieTagInterface>("movie_tag", "movie_id", movieId)
            .then(async result => {
                const tagIds = result.map(ii => ii.tag_id);

                const tags: TagModel[] = [];

                for (let tagId of tagIds) {
                    const tag = await this.getById(tagId, options);
                    tags.push(tag);
                }

                resolve(tags);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    public async getAllByEpisodeId(episodeId: number, options: TagAdapterOptions = {}): Promise<TagModel[]> {
        return new Promise((resolve, reject) => {
            this.getAllFromTableByFieldNameAndValue<EpisodeTagInterface>("episode_tag", "episode_id", episodeId)
            .then(async result => {
                const tagIds = result.map(ii => ii.tag_id);

                const tags: TagModel[] = [];

                for (let tagId of tagIds) {
                    const tag = await this.getById(tagId, options);
                    tags.push(tag);
                }

                resolve(tags);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    public async getAllByTvShowId(tvShowId: number, options: TagAdapterOptions = {}): Promise<TagModel[]> {
        return new Promise((resolve, reject) => {
            this.getAllFromTableByFieldNameAndValue<TvShowTagInterface>("tv_show_tag", "tv_show_id", tvShowId)
            .then(async result => {
                const tagIds = result.map(ii => ii.tag_id);

                const tags: TagModel[] = [];

                for (let tagId of tagIds) {
                    const tag = await this.getById(tagId, options);
                    tags.push(tag);
                }

                resolve(tags);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
}
