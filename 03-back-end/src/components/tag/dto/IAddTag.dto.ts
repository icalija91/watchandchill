import Ajv from "ajv";
import IServiceData from "../../../common/IServiceData.interface";

const ajv = new Ajv();

export default interface IAddTag extends IServiceData {
  name: string;
}

const AddTagSchema = {
  type: "object",
  properties: {
    name: {
      type: "string",
      minLength: 3,
      maxLength: 128,
    },
  },
  required: ["name"],
  additionalProperties: false,
};

const AddTagValidator = ajv.compile(AddTagSchema);

export { AddTagValidator };
