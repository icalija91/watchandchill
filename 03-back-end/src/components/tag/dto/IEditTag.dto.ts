import Ajv from "ajv";
import IServiceData from "../../../common/IServiceData.interface";

const ajv = new Ajv();

export default interface IEditTag extends IServiceData {
  name: string;
}

const EditTagSchema = {
  type: "object",
  properties: {
    name: {
      type: "string",
      minLength: 3,
      maxLength: 128,
    },
  },
  required: ["name"],
  additionalProperties: false,
};

const EditTagValidator = ajv.compile(EditTagSchema);

export { EditTagValidator };
