import IModel from "../../common/IModelInterface.interface";
import TagModel from "../tag/TagModel.model";

class EpisodeModel implements IModel {
  episodeId: number;
  name: string;
  number: number;
  synopsis: string;
  imageUrl: string;
  seasonNumber: number;
  episodeNumber: number;

  tagIds?: TagModel[] = [];
  //FK
  tvShowId: number;
}

export default EpisodeModel;
