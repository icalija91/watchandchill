import BaseService from "../../common/BaseService";
import IAdapterOptions from "../../common/IAdapterOptions.interface";
import { IAddEpisodeDto, IEpisodeTag } from "./dto/IAddEpisode.dto";
import { IEditEpisodeDto } from "./dto/IEditEpisode.dto";
import EpisodeModel from "./EpisodeModel.model";

export interface IEpisodeAdapterOptions extends IAdapterOptions {
  loadTags: boolean;
}

export const DefaultEpisodeAdapterOptions: IEpisodeAdapterOptions = {
  loadTags: true,
};

interface EpisodeUserInterface {
  episode_user_id: number;
  episode_id: number;
  user_id: number;
  rating: number;
  status: Enumerator;
}
export default class IEpisodeService extends BaseService<
  EpisodeModel,
  IEpisodeAdapterOptions
> {
  tableName(): string {
    return "episode";
  }

  protected async adaptToModel(
    data: any,
    options: IEpisodeAdapterOptions
  ): Promise<EpisodeModel> {
    return new Promise(async (resolve) => {
      const episode: EpisodeModel = new EpisodeModel();

      episode.episodeId = +data?.episode_id;
      episode.name = data?.name;
      episode.number = data?.number;
      episode.synopsis = data?.synopsis;
      episode.imageUrl = data?.image_url;
      episode.seasonNumber = data?.season_number;
      episode.episodeNumber = data?.episode_number;

      episode.tvShowId = data?.tv_show_id;

      if (options.loadTags) {
        episode.tagIds = await this.services.tag.getAllByEpisodeId(
          episode.episodeId,
          {}
        );
      }

      resolve(episode);
    });
  }

  public async getAllByTvShowId(
    tvShowId: number,
    options: IEpisodeAdapterOptions
  ): Promise<EpisodeModel[]> {
    return this.getAllByFieldNameAndValue("tv_show_id", tvShowId, options);
  }

  public async add(data: IAddEpisodeDto): Promise<EpisodeModel> {
    return new Promise<EpisodeModel>((resolve, reject) => {
      const sql: string =
        "INSERT `episode` SET `tv_show_id` =?, `name` =?, `number` =?, `synopsis` =?, `image_url` =?, `season_number` =?, `episode_number` =?;";

      this.db
        .execute(sql, [
          data.tvShowId,
          data.name,
          data.number,
          data.synopsis,
          data.imageUrl,
          data.seasonNumber,
          data.episodeNumber,
        ])
        .then(async (result) => {
          const info: any = result;

          const newEpisodeId = +info[0]?.insertId;

          const newEpisode: EpisodeModel | null = await this.getById(
            newEpisodeId,
            {
              loadTags: true,
            }
          );

          if (newEpisode === null) {
            return reject({
              message: "Duplicate episode title in this genre!",
            });
          }

          resolve(newEpisode);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async addEpisodeTag(data: IEpisodeTag): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string = "INSERT episode_tag SET episode_id = ?, tag_id = ?;";

      this.db
        .execute(sql, [data.episode_id, data.tag_id])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async edit(
    episodeId: number,
    data: IEditEpisodeDto,
    options: IEpisodeAdapterOptions
  ): Promise<EpisodeModel> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "UPDATE `episode` SET `tv_show_id` =?, `name` =?, `number` =?, `synopsis` =?, `image_url` =?, `season_number` =?, `episode_number` =? WHERE `episode_id` =?;";
      this.db
        .execute(sql, [
          data.tvShowId,
          data.name,
          data.number,
          data.synopsis,
          data.imageUrl,
          data.seasonNumber,
          data.episodeNumber,
          episodeId,
        ])
        .then(async (result) => {
          const info: any = result;

          if (info[0]?.affectedRows === 0) {
            return reject({ message: "Could not edit episode!" });
          }

          const episode: EpisodeModel | null = await this.getById(
            episodeId,
            options
          );

          if (episode === null) {
            return reject({ message: "Could not find this episode!" });
          }

          resolve(episode);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async deleteEpisodeTag(data: IEpisodeTag): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "DELETE FROM episode_tag WHERE episode_id = ? AND tag_id = ?;";

      this.db
        .execute(sql, [data.episode_id, data.tag_id])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.affectedRows);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async deleteById(episodeId: number): Promise<true> {
    return this.baseDeleteById(episodeId);
  }

  public async getStatusByEpisodeId(
    episodeId: number,
    userId: number,
    options: IEpisodeAdapterOptions
  ): Promise<Enumerator> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValues<EpisodeUserInterface>(
        "episode_user",
        "episode_id",
        episodeId,
        "user_id",
        userId,
        options
      )
        .then(async (result) => {
          const statuses = result.map((ii) => ii.status);
          if (statuses[0] !== undefined) {
            resolve(statuses[0]);
          }
          //user did not set status
          resolve(null);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getRatingByEpisodeId(
    episodeId: number,
    userId: number,
    options: IEpisodeAdapterOptions
  ): Promise<number> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValues<EpisodeUserInterface>(
        "episode_user",
        "episode_id",
        episodeId,
        "user_id",
        userId,
        options
      )
        .then(async (result) => {
          const ratings = result.map((ii) => ii.rating);
          if (ratings[0] !== undefined) {
            resolve(ratings[0]);
          }
          //user did not rate
          resolve(0);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
