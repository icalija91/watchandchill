import { Request, Response } from "express";
import BaseController from "../../common/BaseController";
import { AddTvShowValidator, IAddTvShowDto } from "./dto/IAddTvShow.dto";
import { DefaultTvShowAdapterOptions } from "./TvShowService.service";
import { EditTvShowValidator, IEditTvShowDto } from "./dto/IEditTvShow.dto";
import { DefaultTagAdapterOptions } from "../tag/TagService.service";
import { DefaultEpisodeAdapterOptions } from "./EpisodeService.service";
import IAddEpisodeDto, { AddEpisodeValidator } from "./dto/IAddEpisode.dto";
import { IEditEpisodeDto, EditEpisodeValidator } from "./dto/IEditEpisode.dto";
import { IRateDto, RateValidator } from "../../common/dto/IRate.dto";
import { IStatusDto, StatusValidator } from "../../common/dto/IStatus.dto";

class TvShowController extends BaseController {
  async getAll(req: Request, res: Response) {
    this.services.tvShow
      .getAll(DefaultTvShowAdapterOptions)
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async getRecommended(req: Request, res: Response){
    const userId: number = req.authorisation?.id;
    this.services.tvShow.
    getRecommended(userId)
    .then((result) => {
      res.send(result);
    })
    .catch((error) => {
      res.status(500).send(error?.message);
    });
  }

  async getById(req: Request, res: Response) {
    const id: number = +req.params?.id;

    this.services.tvShow
      .getById(id, DefaultTvShowAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.sendStatus(404);
        }

        res.send(result);
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async add(req: Request, res: Response) {
    const data = req.body as IAddTvShowDto;

    if (!AddTvShowValidator(data)) {
      return res.status(400).send(AddTvShowValidator.errors);
    }

    this.services.tvShow
      .add({
        title: data.title,
        serbianTitle: data.serbianTitle,
        director: data.director,
        synopsis: data.synopsis,
        imageUrl: data.imageUrl,
        category: data.category,
        tagIds: [],
      })
      .then((newTvShow) => {
        for (let givenTagId of data.tagIds) {
          this.services.tvShow
            .addTvShowTag({
              tv_show_id: newTvShow.tvShowId,
              tag_id: givenTagId,
            })
            .catch((error) => {
              throw {
                status: 500,
                message: error?.message,
              };
            });
        }

        return newTvShow;
      })
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(400).send(error?.message);
      });
  }

  async edit(req: Request, res: Response) {
    const tvShowId: number = +req.params?.id;
    const data = req.body as IEditTvShowDto;

    if (!EditTvShowValidator(data)) {
      return res.status(400).send(EditTvShowValidator.errors);
    }

    this.services.tvShow
      .edit(
        tvShowId,
        {
          title: data.title,
          serbianTitle: data.serbianTitle,
          director: data.director,
          synopsis: data.synopsis,
          imageUrl: data.imageUrl,
          category: data.category,
          tagIds: [],
        },
        {
          loadTags: true,
          loadEpisodes: false,
        }
      )
      .then(async (result) => {
        const currentTagId = result.tagIds?.map((tag) => tag.tagId);
        const newTagIds = data.tagIds;

        const availableTags = await this.services.tag.getAllIds(
          "tag_id",
          DefaultTagAdapterOptions
        );
        const availableTagIds = availableTags.map((tag) => tag.tagId);
        for (let id of newTagIds) {
          if (!availableTagIds.includes(id)) {
            throw {
              status: 400,
              message: "Tag " + id + " is not available!",
            };
          }
        }

        const tagIdsToAdd = newTagIds.filter(
          (id) => !currentTagId.includes(id)
        );
        for (let id of tagIdsToAdd) {
          if (
            !(await this.services.tvShow.addTvShowTag({
              tv_show_id: result.tvShowId,
              tag_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error adding a new tag to this tv show!",
            };
          }
        }

        const tagIdsToDelete = currentTagId.filter(
          (id) => !newTagIds.includes(id)
        );
        for (let id of tagIdsToDelete) {
          if (
            !(await this.services.tvShow.deleteTvShowTag({
              tv_show_id: result.tvShowId,
              tag_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error deleting an existing tag from this tv show!",
            };
          }
        }

        return result;
      })
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(400).send(error?.message);
      });
  }

  async delete(req: Request, res: Response) {
    const tvShowId: number = +req.params?.id;

    this.services.tvShow
      .getById(tvShowId, DefaultTvShowAdapterOptions)
      .then((result) => {
        if (result === null)
          throw { status: 404, message: "TV show not found!" };
        return result;
      })
      .then(async (result) => {
        const currentTagId = result.tagIds?.map((tag) => tag.tagId);

        for (let id of currentTagId) {
          if (
            !(await this.services.tvShow.deleteTvShowTag({
              tv_show_id: result.tvShowId,
              tag_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error deleting an existing tag from this tv show!",
            };
          }
        }

        return result;
      })
      .then((tvShow) => {
        return this.services.tvShow.deleteById(tvShow.tvShowId);
      })
      .then(() => {
        res.send("Deleted!");
      })
      .catch((error) => {
        res
          .status(error?.status ?? 500)
          .send(error?.message ?? "Server side error!");
      });
  }

  async getAllEpisodesByTvShowId(req: Request, res: Response) {
    const tvShowId: number = +req.params?.id;

    this.services.tvShow
      .getById(tvShowId, DefaultTvShowAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.status(404).send("TV Show not found!");
        }

        this.services.episode
          .getAllByTvShowId(tvShowId, {
            loadTags: false,
          })
          .then((result) => {
            result.sort((a, b) => {
              if (a.seasonNumber < b.seasonNumber) return -1;
              if (a.seasonNumber > b.seasonNumber) return 1;
              return 0;
            });
            res.send(result);
          })
          .catch((error) => {
            res.status(500).send(error?.message);
          });
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async getEpisodeById(req: Request, res: Response) {
    const id: number = +req.params?.eid;

    this.services.episode
      .getById(id, DefaultEpisodeAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.sendStatus(404);
        }

        res.send(result);
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async addEpisode(req: Request, res: Response) {
    const tvShowId: number = +req.params?.id;
    const data = req.body as IAddEpisodeDto;

    if (!AddEpisodeValidator(data)) {
      return res.status(400).send(AddEpisodeValidator.errors);
    }

    this.services.tvShow
      .getById(tvShowId, { loadEpisodes: false, loadTags: false })
      .then((result) => {
        if (result === null) {
          throw {
            status: 404,
            message: "Episode not found!",
          };
        }
      })
      .then(() => {
        return this.services.episode.add({
          name: data.name,
          number: data.number,
          synopsis: data.synopsis,
          imageUrl: data.imageUrl,
          seasonNumber: data.seasonNumber,
          episodeNumber: data.episodeNumber,
          tagIds: data.tagIds,
          tvShowId: tvShowId,
        });
      })
      .then((newEpisode) => {
        for (let givenTagId of data.tagIds) {
          this.services.episode
            .addEpisodeTag({
              episode_id: newEpisode.episodeId,
              tag_id: givenTagId,
            })
            .catch((error) => {
              throw {
                status: 500,
                message: error?.message,
              };
            });
        }

        return newEpisode;
      })
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(error?.status ?? 500).send(error?.message);
      });
  }

  async editEpisode(req: Request, res: Response) {
    const tvShowId: number = +req.params?.id;
    const episodeId: number = +req.params?.eid;
    const data: IEditEpisodeDto = req.body as IEditEpisodeDto;

    if (!EditEpisodeValidator(data)) {
      return res.status(400).send(EditEpisodeValidator.errors);
    }
    this.services.episode
      .getById(episodeId, { loadTags: false })
      .then((result) => {
        if (!result) {
          throw {
            status: 404,
            message: "Episode not found!",
          };
        }
        return result;
      })
      .then((episode) => {
        if (episode.tvShowId !== tvShowId) {
          throw {
            status: 400,
            message: "This episode does not belong to this tv show!",
          };
        }
        return episode;
      })
      .then((episode) => {
        return this.services.episode.edit(
          episodeId,
          {
            name: data.name,
            number: data.number,
            synopsis: data.synopsis,
            imageUrl: data.imageUrl,
            seasonNumber: data.seasonNumber,
            episodeNumber: data.episodeNumber,
            tagIds: [],
            tvShowId: tvShowId,
          },
          {
            loadTags: true,
          }
        );
      })
      .then(async (result) => {
        const currentTagId = result.tagIds?.map((tag) => tag.tagId);
        const newTagIds = data.tagIds;

        const availableTags = await this.services.tag.getAllIds(
          "tag_id",
          DefaultTagAdapterOptions
        );
        const availableTagIds = availableTags.map((tag) => tag.tagId);
        for (let id of newTagIds) {
          if (!availableTagIds.includes(id)) {
            throw {
              status: 400,
              message: "Tag " + id + " is not available!",
            };
          }
        }

        const tagIdsToAdd = newTagIds.filter(
          (id) => !currentTagId.includes(id)
        );
        for (let id of tagIdsToAdd) {
          if (
            !(await this.services.episode.addEpisodeTag({
              episode_id: result.episodeId,
              tag_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error adding a new tag to this episode!",
            };
          }
        }

        const episodeIdsToDelete = currentTagId.filter(
          (id) => !newTagIds.includes(id)
        );
        for (let id of episodeIdsToDelete) {
          if (
            !(await this.services.episode.deleteEpisodeTag({
              episode_id: result.episodeId,
              tag_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error deleting an existing tag from this episode!",
            };
          }
        }

        return result;
      })
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(error?.status ?? 500).send(error?.message);
      });
  }

  async deleteEpisode(req: Request, res: Response) {
    const tvShowId: number = +req.params?.id;
    const episodeId: number = +req.params?.eid;

    this.services.tvShow
      .getById(tvShowId, { loadEpisodes: false, loadTags: false })
      .then((result) => {
        if (result === null) {
          throw {
            status: 404,
            message: "Tv show not found!",
          };
        }
      })
      .then(() => {
        return this.services.episode.getById(episodeId, { loadTags: true });
      })
      .then((result) => {
        if (result === null) {
          throw {
            status: 404,
            message: "Episode not found!",
          };
        }

        return result;
      })
      .then((result) => {
        if (result.tvShowId !== tvShowId) {
          throw {
            status: 400,
            message: "This episode does not belong to this tv show!",
          };
        }
        return result;
      })
      .then(async (result) => {
        const currentTagId = result.tagIds?.map((tag) => tag.tagId);

        for (let id of currentTagId) {
          if (
            !(await this.services.episode.deleteEpisodeTag({
              episode_id: result.episodeId,
              tag_id: id,
            }))
          ) {
            throw {
              status: 500,
              message: "Error deleting an existing tag from this episode!",
            };
          }
        }

        return result;
      })
      .then(() => {
        return this.services.episode.deleteById(episodeId);
      })
      .then(() => {
        res.send("This episode has been deleted!");
      })
      .catch((error) => {
        res.status(error?.status ?? 500).send(error?.message);
      });
  }

  public async rate(req: Request, res: Response) {
    const tvShowId = +req.params?.id;
    const userId = req.authorisation?.id;
    const data = req.body as IRateDto;

    if (!RateValidator(data)) {
      return res.status(400).send(RateValidator.errors);
    }

    this.services.tvShow
      .rate(tvShowId, userId, {
        mark_value: "" + data.value,
      })
      .then(() => {
        this.services.user.updateRatingFrequency(userId);
      })
      .then(() => {
        res.send("Rating saved");
      })
      .catch((error) => {
        res.status(error?.status ?? 500).send(error?.message);
      });
  }

  public async status(req: Request, res: Response) {
    const tvShowId = +req.params?.id;
    const userId = req.authorisation?.id;
    const data = req.body as IStatusDto;

    if (!StatusValidator(data)) {
      return res.status(400).send(StatusValidator.errors);
    }

    this.services.tvShow
      .status(tvShowId, userId, {
        mark_value: "" + data.value,
      })
      .then(() => {
        res.send("Status saved");
      })
      .catch((error) => {
        res.status(error?.status ?? 500).send(error?.message);
      });
  }

  public async rateEpisode(req: Request, res: Response) {
    const episodeId = +req.params?.eid;
    const userId = req.authorisation?.id;
    const data = req.body as IRateDto;

    if (!RateValidator(data)) {
      return res.status(400).send(RateValidator.errors);
    }

    this.services.tvShow
      .rateEpisode(episodeId, userId, {
        mark_value: "" + data.value,
      })
      .then(() => {
        this.services.user.updateRatingFrequency(userId);
      })
      .then(() => {
        res.send("Rating saved");
      })
      .catch((error) => {
        res.status(error?.status ?? 500).send(error?.message);
      });
  }

  public async statusEpisode(req: Request, res: Response) {
    const episodeId = +req.params?.eid;
    const userId = req.authorisation?.id;
    const data = req.body as IStatusDto;

    if (!StatusValidator(data)) {
      return res.status(400).send(StatusValidator.errors);
    }

    this.services.tvShow
      .statusEpisode(episodeId, userId, {
        mark_value: "" + data.value,
      })
      .then(() => {
        res.send("Status saved");
      })
      .catch((error) => {
        res.status(error?.status ?? 500).send(error?.message);
      });
  }

  async getStatusByTvShowId(req: Request, res: Response) {
    const tvShowId: number = +req.params?.id;
    const userId: number = req.authorisation?.id;

    this.services.tvShow
      .getStatusByTvShowId(tvShowId, userId, DefaultTvShowAdapterOptions)
      .then((result) => {
        res.send(String(result));
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async getStatusByEpisodeId(req: Request, res: Response) {
    const episodeId: number = +req.params?.eid;
    const userId: number = req.authorisation?.id;

    this.services.episode
      .getStatusByEpisodeId(episodeId, userId, DefaultEpisodeAdapterOptions)
      .then((result) => {
        res.send(String(result));
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async getRatingByTvShowIdAndUserId(req: Request, res: Response) {
    const tvShowId: number = +req.params?.id;
    const userId: number = req.authorisation?.id;

    this.services.tvShow
      .getRatingByTvShowIdAndUserId(tvShowId, userId, DefaultTvShowAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.sendStatus(404);
        }
        res.send(String(result));
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }

  async getRatingByEpisodeId(req: Request, res: Response) {
    const episodeId: number = +req.params?.eid;
    const userId: number = req.authorisation?.id;

    this.services.episode
      .getRatingByEpisodeId(episodeId, userId, DefaultEpisodeAdapterOptions)
      .then((result) => {
        if (result === null) {
          return res.sendStatus(404);
        }
        res.send(String(result));
      })
      .catch((error) => {
        res.status(500).send(error?.message);
      });
  }
}

export default TvShowController;
