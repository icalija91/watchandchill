import IModel from "../../common/IModelInterface.interface";
import TagModel from "../tag/TagModel.model";
import EpisodeModel from "./EpisodeModel.model";

class TvShowModel implements IModel {
  tvShowId: number;
  title: string;
  serbianTitle: string;
  director: string;
  synopsis: string;
  imageUrl: string;
  category: Enumerator;
  tagIds: TagModel[];
  episodes?: EpisodeModel[];
}

export default TvShowModel;
