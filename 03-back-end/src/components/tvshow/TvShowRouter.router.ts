
import * as express from "express";
import IApplicationResources from '../../common/IApplicationResources.interface';
import IRouter from '../../common/IRouter.interface';
import AuthMiddleware from "../../middlewares/AuthMiddleware";
import TvShowController from "./TvShowController.controller";

class TvShowRouter implements IRouter{

    public setupRoutes(application: express.Application, resources: IApplicationResources){
        
        const tvShowController: TvShowController = new TvShowController(resources.services);

        application.get("/api/tvshow", AuthMiddleware.getVerifier("administrator","user") , tvShowController.getAll.bind(tvShowController));
        application.get("/api/tvshow/recommended",           AuthMiddleware.getVerifier("user"), tvShowController.getRecommended.bind(tvShowController));
        application.get("/api/tvshow/:id", AuthMiddleware.getVerifier("administrator","user"),  tvShowController.getById.bind(tvShowController));
        application.post("/api/tvshow",AuthMiddleware.getVerifier("administrator"),  tvShowController.add.bind(tvShowController));
        application.put("/api/tvshow/:id",AuthMiddleware.getVerifier("administrator"),  tvShowController.edit.bind(tvShowController));
        application.delete("/api/tvshow/:id", AuthMiddleware.getVerifier("administrator"), tvShowController.delete.bind(tvShowController));
        application.get("/api/tvshow/:id/episode/:eid", AuthMiddleware.getVerifier("administrator","user"), tvShowController.getEpisodeById.bind(tvShowController));
        application.get("/api/tvshow/:id/episode", AuthMiddleware.getVerifier("administrator","user"), tvShowController.getAllEpisodesByTvShowId.bind(tvShowController));
        application.post("/api/tvshow/:id/episode", AuthMiddleware.getVerifier("administrator"), tvShowController.addEpisode.bind(tvShowController));
        application.put("/api/tvshow/:id/episode/:eid", AuthMiddleware.getVerifier("administrator"), tvShowController.editEpisode.bind(tvShowController));
        application.delete("/api/tvshow/:id/episode/:eid", AuthMiddleware.getVerifier("administrator"), tvShowController.deleteEpisode.bind(tvShowController));
        application.post("/api/tvshow/:id/rate",           AuthMiddleware.getVerifier("user"), tvShowController.rate.bind(tvShowController));
        application.post("/api/tvshow/:id/episode/:eid/rate",           AuthMiddleware.getVerifier("user"), tvShowController.rateEpisode.bind(tvShowController));
        application.post("/api/tvshow/:id/status",           AuthMiddleware.getVerifier("user"), tvShowController.status.bind(tvShowController));
        application.post("/api/tvshow/:id/episode/:eid/status",           AuthMiddleware.getVerifier("user"), tvShowController.statusEpisode.bind(tvShowController));
        application.get("/api/tvshow/:id/status",           AuthMiddleware.getVerifier("user"), tvShowController.getStatusByTvShowId.bind(tvShowController));
        application.get("/api/tvshow/:id/episode/:eid/status",           AuthMiddleware.getVerifier("user"), tvShowController.getStatusByEpisodeId.bind(tvShowController));
        application.get("/api/tvshow/:id/rate",           AuthMiddleware.getVerifier("user"), tvShowController.getRatingByTvShowIdAndUserId.bind(tvShowController));
        application.get("/api/tvshow/:id/episode/:eid/rate",           AuthMiddleware.getVerifier("user"), tvShowController.getRatingByEpisodeId.bind(tvShowController));

        

    }
}

export default TvShowRouter;