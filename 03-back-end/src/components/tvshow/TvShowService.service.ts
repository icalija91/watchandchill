import BaseService from "../../common/BaseService";
import IAdapterOptions from "../../common/IAdapterOptions.interface";
import { IRate } from "../../common/dto/IRate.dto";
import { IStatus } from "../../common/dto/IStatus.dto";
import TvShowModel from "./TvShowModel.model";
import { IAddTvShowDto, ITvShowTag } from "./dto/IAddTvShow.dto";
import { IEditTvShowDto } from "./dto/IEditTvShow.dto";
import TagModel from "../tag/TagModel.model";
import { TvShowTagInterface } from "../tag/TagService.service";
import { fileURLToPath } from "url";

export interface ITvShowAdapterOptions extends IAdapterOptions {
  loadTags: boolean;
  loadEpisodes: boolean;
}

export const DefaultTvShowAdapterOptions: ITvShowAdapterOptions = {
  loadTags: true,
  loadEpisodes: true,
};

interface TvShowUserInterface {
  tv_show_user_id: number;
  tv_show_id: number;
  user_id: number;
  rating: number;
  status: Enumerator;
}

export interface ITvShowWeights {
  tvShowId: number;
  weight: number;
}
export default class ITvShowService extends BaseService<
  TvShowModel,
  ITvShowAdapterOptions
> {
  tableName(): string {
    return "tv_show";
  }

  protected async adaptToModel(
    data: any,
    options: ITvShowAdapterOptions
  ): Promise<TvShowModel> {
    return new Promise(async (resolve) => {
      const tvShow: TvShowModel = new TvShowModel();

      tvShow.tvShowId = +data?.tv_show_id;
      tvShow.title = data?.title;
      tvShow.serbianTitle = data?.serbian_title;
      tvShow.director = data?.director;
      tvShow.synopsis = data?.synopsis;
      tvShow.imageUrl = data?.image_url;
      tvShow.category = data?.category;

      if (options.loadTags) {
        tvShow.tagIds = await this.services.tag.getAllByTvShowId(
          tvShow.tvShowId,
          {}
        );
      }
      if (options.loadEpisodes) {
        tvShow.episodes = await this.services.episode.getAllByTvShowId(
          tvShow.tvShowId,
          {
            loadTags: false,
          }
        );
      }

      resolve(tvShow);
    });
  }

  public async getRecommended(userId: number): Promise<TvShowModel[]> {
    let recommendedTvShows = [];
    let filteredTvShows = await this.getTvShowsBasedOnUserPreference(userId);
    if (filteredTvShows.length !== 0) {
      let weights = await this.calculateWeights(filteredTvShows);
      recommendedTvShows = await this.getRecommendedTvShows(
        filteredTvShows,
        weights
      );
    } else {
      let allTvShows = await this.getAll(DefaultTvShowAdapterOptions);
      recommendedTvShows = allTvShows.slice(0, 10);
    }
    return recommendedTvShows;
  }

  public async getRecommendedTvShows(
    filteredTvShows: number[],
    weights: number[]
  ): Promise<TvShowModel[]> {
    let tvShowWeights = [];
    //Create a list containing tvShowId/weight objects
    for (let i = 0; i < filteredTvShows.length; i++) {
      let tvShowWeight: ITvShowWeights = {
        tvShowId: filteredTvShows[i],
        weight: weights[i],
      };
      tvShowWeights.push(tvShowWeight);
    }
    // Sort the array based on weights by descending order
    tvShowWeights.sort((a, b) => b.weight - a.weight);
    //get top 10
    const topTenTvShowArray = tvShowWeights.slice(0, 10);

    let promiseTvShows = [];
    topTenTvShowArray.forEach((tvShowWeight) => {
      promiseTvShows.push(
        this.services.tvShow.getById(
          tvShowWeight.tvShowId,
          DefaultTvShowAdapterOptions
        )
      );
    });
    let tvShows = await Promise.all(promiseTvShows);

    return tvShows;
  }

  public async calculateWeights(filteredTvShows: number[]): Promise<number[]> {
    const ratingsAndUsers = await Promise.all(
      filteredTvShows.map((tvShowId) =>
        this.services.tvShow.getRatingByTvShowId(
          tvShowId,
          DefaultTvShowAdapterOptions
        )
      )
    );
    const weights = [];
    for (const ratingAndUser of ratingsAndUsers) {
      const ratingFrequency = await Promise.all(
        ratingAndUser.map((tv) =>
          this.services.user.getRatingFrequency(tv.user_id)
        )
      );
      let rating = ratingAndUser.map((tv) => tv.rating);

      let tvShowWeights = this.weightedMean(rating, ratingFrequency);
      weights.push(tvShowWeights);
    }
    return weights;
  }

  public weightedMean(ratings, ratingFrequency) {
    // Calculate the weighted mean
    let sum = 0;
    let totalVotes = 0;
    let weightedMean = [];

    for (let i = 0; i < ratings.length; i++) {
      const weightedRating = ratings[i] * ratingFrequency[i];
      sum += weightedRating;
      totalVotes += ratingFrequency[i];
    }
    weightedMean.push(sum / totalVotes);
    return weightedMean;
  }

  public async getTvShowsBasedOnUserPreference(
    userId: number
  ): Promise<number[]> {
    let tagsToFilterOut =
      await this.getAllTagsWhereStatusDontWantToWatchOrLowerRating(userId);
    let tagsToFilterIn = await this.getAllTagsWhereStatusWatchedAndHighRating(
      userId
    );
    let filteredTvShows = await this.getAllTvShowsBasedOnTags(
      tagsToFilterIn,
      tagsToFilterOut,
      userId
    );
    return filteredTvShows;
  }

  public async getAllTvShowsBasedOnTags(
    tagsToFilterIn: TagModel[],
    tagsToFilterOut: TagModel[],
    userId: number
  ): Promise<number[]> {
    let positiveTagIds = tagsToFilterIn.map((tag) => tag.tagId);
    let negativeTagIds = tagsToFilterOut.map((tag) => tag.tagId);

    let positiveTvShows = [];
    let negativeTvShows = [];
    if (positiveTagIds.length !== 0) {
      let positiveArray = await this.getAllTvShowsIdsBasedOnTags(
        positiveTagIds
      );
      //filter out duplicates
      positiveTvShows = Array.from(new Set(positiveArray));
    }
    if (positiveTagIds.length !== 0) {
      let negativeArray = await this.getAllTvShowsIdsBasedOnTags(
        negativeTagIds
      );
      //filter out duplicates
      negativeTvShows = Array.from(new Set(negativeArray));
    }
    let userTvShows = await this.getAllTvShowsByUserId(userId);

    let tvShowsToFilterOut = negativeTvShows.concat(userTvShows);
    //return only tv shows that user didnt rate/set status and which dont include negative tags
    return positiveTvShows.filter((el) => !tvShowsToFilterOut.includes(el));
  }

  public async getAllTvShowsByUserId(userId: number): Promise<number[]> {
    return new Promise((resolve, reject) => {
      this.getAllFromTableByFieldNameAndValue<TvShowUserInterface>(
        "tv_show_user",
        "user_id",
        userId
      )
        .then((result) => {
          const tvShowIds = result.map((ii) => ii.tv_show_id);
          resolve(tvShowIds);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getAllTvShowsIdsBasedOnTags(
    tagIds: number[]
  ): Promise<number[]> {
    return new Promise((resolve, reject) => {
      this.getAllFromTableByFieldValuesArray<TvShowTagInterface>(
        "tv_show_tag",
        tagIds
      )
        .then((result) => {
          const tvShowIds = result.map((ii) => ii.tv_show_id);
          resolve(tvShowIds);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getAllTagsWhereStatusWatchedAndHighRating(
    userId: number
  ): Promise<TagModel[]> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValuesWhereRatingHigh<TvShowUserInterface>(
        "tv_show_user",
        "status",
        "watched",
        "user_id",
        userId,
        DefaultTvShowAdapterOptions
      )
        .then(async (result) => {
          let tvShowIds = result.map((tv) => tv.tv_show_id);
          if (tvShowIds[0] !== undefined) {
            const filteredTagIds = await Promise.all(
              tvShowIds.map((tvShowId) =>
                this.services.tag.getAllByTvShowId(tvShowId, {})
              )
            );
            resolve(filteredTagIds.flat());
          } else {
            resolve([]);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getAllTagsWhereStatusDontWantToWatchOrLowerRating(
    userId: number
  ): Promise<TagModel[]> {
    let tagsLowerRating = await this.getAllTagsWithLowRating(userId);
    let tagsDontWantToWatch = await this.getAllTagsWhereStatusDontWantToWatch(
      userId
    );
    return [...tagsLowerRating, ...tagsDontWantToWatch].flat();
  }

  public async getAllTagsWhereStatusDontWantToWatch(
    userId: number
  ): Promise<TagModel[]> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValues<TvShowUserInterface>(
        "tv_show_user",
        "status",
        "dont_want_to_watch",
        "user_id",
        userId,
        DefaultTvShowAdapterOptions
      )
        .then(async (result) => {
          let tvShowIds = result.map((tv) => tv.tv_show_id);
          if (tvShowIds[0] !== undefined) {
            const filteredTagIds = await Promise.all(
              tvShowIds.map((tvShowId) =>
                this.services.tag.getAllByTvShowId(tvShowId, {})
              )
            );
            resolve(filteredTagIds.flat());
          } else {
            resolve([]);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getAllTagsWithLowRating(userId: number): Promise<TagModel[]> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValuesLessOrEqual<TvShowUserInterface>(
        "tv_show_user",
        "rating",
        5,
        "user_id",
        userId,
        DefaultTvShowAdapterOptions
      )
        .then(async (result) => {
          let tvShowIds = result.map((tv) => tv.tv_show_id);
          if (tvShowIds[0] !== undefined) {
            const filteredTagIds = await Promise.all(
              tvShowIds.map((tvShowId) =>
                this.services.tag.getAllByTvShowId(tvShowId, {})
              )
            );
            resolve(filteredTagIds.flat());
          } else {
            resolve([]);
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async add(data: IAddTvShowDto): Promise<TvShowModel> {
    return new Promise<TvShowModel>((resolve, reject) => {
      const sql: string =
        "INSERT `tv_show` SET `title` =?, `serbian_title` =?, `director` =?, `synopsis` =?, `image_url` =?, `category` =?;";

      this.db
        .execute(sql, [
          data.title,
          data.serbianTitle,
          data.director,
          data.synopsis,
          data.imageUrl,
          data.category,
        ])
        .then(async (result) => {
          const info: any = result;

          const newTvShowId = +info[0]?.insertId;

          const newTvShow: TvShowModel | null = await this.getById(
            newTvShowId,
            {
              loadTags: true,
              loadEpisodes: true,
            }
          );

          if (newTvShow === null) {
            return reject({ message: "Duplicate tv show title!" });
          }

          resolve(newTvShow);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async addTvShowTag(data: ITvShowTag): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string = "INSERT tv_show_tag SET tv_show_id = ?, tag_id = ?;";

      this.db
        .execute(sql, [data.tv_show_id, data.tag_id])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async edit(
    tvShowId: number,
    data: IEditTvShowDto,
    options: ITvShowAdapterOptions
  ): Promise<TvShowModel> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "UPDATE `tv_show` SET `title` =?, `serbian_title` =?, `director` =?, `synopsis` =?, `image_url` =?, `category` =? WHERE `tv_show_id` =?;";
      this.db
        .execute(sql, [
          data.title,
          data.serbianTitle,
          data.director,
          data.synopsis,
          data.imageUrl,
          data.category,
          tvShowId,
        ])
        .then(async (result) => {
          const info: any = result;

          if (info[0]?.affectedRows === 0) {
            return reject({ message: "Could not edit tv show!" });
          }

          const tvShow: TvShowModel | null = await this.getById(
            tvShowId,
            options
          );

          if (tvShow === null) {
            return reject({ message: "Could not find this tv show!" });
          }

          resolve(tvShow);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async deleteTvShowTag(data: ITvShowTag): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "DELETE FROM tv_show_tag WHERE tv_show_id = ? AND tag_id = ?;";

      this.db
        .execute(sql, [data.tv_show_id, data.tag_id])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.affectedRows);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async deleteById(tvShowId: number): Promise<true> {
    return this.baseDeleteById(tvShowId);
  }

  async rate(tvShowId: number, userId: number, data: IRate): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "INSERT tv_show_user SET tv_show_id = ?, user_id = ?, rating = ? ON DUPLICATE KEY UPDATE rating = VALUES(rating);";

      this.db
        .execute(sql, [tvShowId, userId, data.mark_value])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getRatingByTvShowId(
    tvShowId: number,
    options: ITvShowAdapterOptions
  ): Promise<TvShowUserInterface[]> {
    return new Promise((resolve, reject) => {
      this.getAllFromTableByFieldNameAndValue<TvShowUserInterface>(
        "tv_show_user",
        "tv_show_id",
        tvShowId
      )
        .then(async (result) => {
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getRatingByTvShowIdAndUserId(
    tvShowId: number,
    userId: number,
    options: ITvShowAdapterOptions
  ): Promise<number> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValues<TvShowUserInterface>(
        "tv_show_user",
        "tv_show_id",
        tvShowId,
        "user_id",
        userId,
        options
      )
        .then(async (result) => {
          const ratings = result.map((ii) => ii.rating);
          if (ratings[0] !== undefined) {
            resolve(ratings[0]);
          }
          //user did not rate
          resolve(0);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async status(
    tvShowId: number,
    userId: number,
    data: IStatus
  ): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "INSERT tv_show_user SET tv_show_id = ?, user_id = ?, status = ? ON DUPLICATE KEY UPDATE status = VALUES(status);";

      this.db
        .execute(sql, [tvShowId, userId, data.mark_value])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  public async getStatusByTvShowId(
    tvShowId: number,
    userId: number,
    options: ITvShowAdapterOptions
  ): Promise<Enumerator> {
    return new Promise((resolve, reject) => {
      this.getAllByFieldNameAndValues<TvShowUserInterface>(
        "tv_show_user",
        "tv_show_id",
        tvShowId,
        "user_id",
        userId,
        options
      )
        .then(async (result) => {
          const statuses = result.map((ii) => ii.status);
          if (statuses[0] !== undefined) {
            resolve(statuses[0]);
          }
          //user did not set status
          resolve(null);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async rateEpisode(
    episodeId: number,
    userId: number,
    data: IRate
  ): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "INSERT episode_user SET episode_id = ?, user_id = ?, rating = ? ON DUPLICATE KEY UPDATE rating = VALUES(rating);";

      this.db
        .execute(sql, [episodeId, userId, data.mark_value])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  async statusEpisode(
    episodeId: number,
    userId: number,
    data: IStatus
  ): Promise<number> {
    return new Promise((resolve, reject) => {
      const sql: string =
        "INSERT episode_user SET episode_id = ?, user_id = ?, status = ? ON DUPLICATE KEY UPDATE status = VALUES(status);";

      this.db
        .execute(sql, [episodeId, userId, data.mark_value])
        .then(async (result) => {
          const info: any = result;
          resolve(+info[0]?.insertId);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
