import Ajv from "ajv";
import IServiceData from "../../../common/IServiceData.interface";

const ajv = new Ajv();

export default interface IAddEpisodeDto {
  name: string;
  number: number;
  synopsis: string;
  imageUrl: string;
  seasonNumber: number;
  episodeNumber: number;
  tagIds: number[];
  tvShowId: number;
}

export interface IEpisodeTag extends IServiceData {
  episode_id: number;
  tag_id: number;
}

const AddEpisodeSchema = {
  type: "object",
  properties: {
    name: {
      type: "string",
      minLength: 1,
      maxLength: 128,
    },
    synopsis: {
      type: "string",
      minLength: 4,
      maxLength: 512,
    },
    imageUrl: {
      type: "string",
      minLength: 4,
      maxLength: 512,
    },
    tagIds: {
      type: "array",
      minItems: 0,
      uniqueItems: true,
      items: { type: "integer" },
    },
  },
  required: ["seasonNumber", "episodeNumber"],
  additionalProperties: true,
};

const AddEpisodeValidator = ajv.compile(AddEpisodeSchema);

export { AddEpisodeValidator, IAddEpisodeDto as IAddEpisodeDto };
