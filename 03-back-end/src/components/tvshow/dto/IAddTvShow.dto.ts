import Ajv from "ajv";
import IServiceData from "../../../common/IServiceData.interface";

const ajv = new Ajv();

interface IAddTvShowDto {
  title: string;
  serbianTitle: string;
  director: string;
  synopsis: string;
  imageUrl: string;
  category: Enumerator;
  tagIds: number[];
}

export interface ITvShowTag extends IServiceData {
  tv_show_id: number;
  tag_id: number;
}

const AddTvShowSchema = {
  type: "object",
  properties: {
    title: {
      type: "string",
      minLength: 1,
      maxLength: 128,
    },
    serbianTitle: {
      type: "string",
      minLength: 1,
      maxLength: 128,
    },
    director: {
      type: "string",
      minLength: 2,
      maxLength: 32,
    },
    synopsis: {
      type: "string",
      minLength: 4,
      maxLength: 512,
    },
    imageUrl: {
      type: "string",
      minLength: 4,
      maxLength: 512,
    },
    category: {
      type: "string",
      enum: ["artistic", "indie", "box-office", "documentary", "silent"],
    },
    tagIds: {
      type: "array",
      minItems: 0,
      uniqueItems: true,
      items: { type: "integer" },
    },
  },
  required: [
    "title",
    "serbianTitle",
    "director",
    "synopsis",
    "imageUrl",
    "category",
  ],
  additionalProperties: false,
};

const AddTvShowValidator = ajv.compile(AddTvShowSchema);

export { AddTvShowValidator, IAddTvShowDto as IAddTvShowDto };
