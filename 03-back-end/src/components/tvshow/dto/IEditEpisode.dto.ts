import Ajv from "ajv";
import IServiceData from "../../../common/IServiceData.interface";

const ajv = new Ajv();

interface IEditEpisodeDto {
  name: string;
  number: number;
  synopsis: string;
  imageUrl: string;
  seasonNumber: number;
  episodeNumber: number;
  tagIds: number[];
  tvShowId: number;
}

const EditEpisodeSchema = {
  type: "object",
  properties: {
    name: {
      type: "string",
      minLength: 1,
      maxLength: 128,
    },
    synopsis: {
      type: "string",
      minLength: 4,
      maxLength: 512,
    },
    imageUrl: {
      type: "string",
      minLength: 4,
      maxLength: 512,
    },
    tagIds: {
      type: "array",
      minItems: 0,
      uniqueItems: true,
      items: { type: "integer" },
    },
  },
  required: ["seasonNumber", "episodeNumber"],
  additionalProperties: true,
};

const EditEpisodeValidator = ajv.compile(EditEpisodeSchema);

export { EditEpisodeValidator, IEditEpisodeDto };
