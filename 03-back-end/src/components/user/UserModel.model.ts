import IModel from "../../common/IModelInterface.interface";

export default class UserModel implements IModel {
  userId: number;
  email: string;
  username: string;
  passwordHash: string | null;
  activationCode: string | null;
  passwordResetCode: string | null;
  ratingFrequency: number | 0;
}
