import Ajv from "ajv";
import addFormats from "ajv-formats";
import IServiceData from "../../../common/IServiceData.interface";

const ajv = new Ajv();
addFormats(ajv);

export default interface IEditUser extends IServiceData {
  password_hash?: string;
  activation_code?: string;
  password_reset_code?: string;
}

export interface IEditUserDto {
  password?: string;
}

const EditUserValidator = ajv.compile({
  type: "object",
  properties: {
    password: {
      type: "string",
      pattern: "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,}$",
    },
  },
  required: [],
  additionalProperties: false,
});

export { EditUserValidator };
