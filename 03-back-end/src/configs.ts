import { readFileSync } from "fs";
import IConfig from "./common/IConfig.interface";
import AdministratorRouter from "./components/administrator/AdministratorRouter.router";
import GenreRouter from "./components/genre/GenreRouter.router";
import MovieRouter from "./components/movie/MovieRouter.router";
import TagRouter from "./components/tag/TagRouter.router";
import TvShowRouter from "./components/tvshow/TvShowRouter.router";
import { MailConfigurationParameters } from "./config.mail";
import UserRouter from "./components/user/UserRouter.router";
import AuthRouter from "./components/auth/AuthRouter.router";

const DevConfig: IConfig = {
  server: {
    port: 10000,
    static: {
      index: false,
      dotfiles: "deny",
      cacheControl: true,
      etag: true,
      maxAge: 1000 * 60 * 60 * 24,
      path: "./static",
      route: "/assets",
    },
  },
  logging: {
    path: "./logs",
    format:
      ":date[iso]\t:remote-addr\t:method\t:url\t:status\t:res[content-lenght] bytes\t:response-time ms",
    filename: "access.log",
  },
  database: {
    host: "localhost",
    port: 3306,
    user: "aplikacija",
    password: "aplikacija",
    database: "watch_and_chill",
    charset: "utf8",
    timezone: "+01:00",
    supportBigNumbers: true,
  },
  routers: [
    new GenreRouter(),
    new AdministratorRouter(),
    new UserRouter(),
    new MovieRouter(),
    new TagRouter(),
    new TvShowRouter(),
    new AuthRouter(),
  ],
  mail: {
    host: "smtp.office365.com",
    port: 587,
    email: "",
    password: "",
    debug: true,
  },
  auth: {
    administrator: {
      algorithm: "RS256",
      issuer: "PIiVT",
      tokens: {
        auth: {
          duration: 60 * 60 * 24,
          keys: {
            public: readFileSync("./.keystore/app.public", "ascii"),
            private: readFileSync("./.keystore/app.private", "ascii"),
          },
        },
        refresh: {
          duration: 60 * 60 * 24 * 60, // Za dev: 60 dana - inace treba oko mesec dana
          keys: {
            public: readFileSync("./.keystore/app.public", "ascii"),
            private: readFileSync("./.keystore/app.private", "ascii"),
          },
        },
      },
    },
    user: {
      algorithm: "RS256",
      issuer: "PIiVT",
      tokens: {
        auth: {
          duration: 60 * 60 * 24,
          keys: {
            public: readFileSync("./.keystore/app.public", "ascii"),
            private: readFileSync("./.keystore/app.private", "ascii"),
          },
        },
        refresh: {
          duration: 60 * 60 * 24 * 60, // Za dev: 60 dana - inace treba oko mesec dana
          keys: {
            public: readFileSync("./.keystore/app.public", "ascii"),
            private: readFileSync("./.keystore/app.private", "ascii"),
          },
        },
      },
    },
    allowAllRoutesWithoutAuthTokens: false, // Samo dok traje razvoj front-end dela bez mogucnosti prijave
  },
};

DevConfig.mail = MailConfigurationParameters;

export { DevConfig };
