import * as express from "express";
import * as cors from "cors";
import IConfig from "./common/IConfig.interface";
import { DevConfig } from "./configs";
import * as fs from "fs";
import * as morgan from "morgan";
import IApplicationResources from "./common/IApplicationResources.interface";
import path = require("path");
import * as mysql2 from "mysql2/promise";
import GenreService from "./components/genre/GenreService.service";
import MovieService from "./components/movie/MovieService.service";
import EpisodeService from "./components/tvshow/EpisodeService.service";
import AdministratorService from "./components/administrator/AdministratorService.service";
import TagService from "./components/tag/TagService.service";
import TvShowService from "./components/tvshow/TvShowService.service";
import UserService from "./components/user/UserService.service";

async function main() {
  const config: IConfig = DevConfig;

  fs.mkdirSync(config.logging.path, {
    mode: 0o755,
    recursive: true,
  });

  const db = await mysql2.createConnection({
    host: config.database.host,
    port: config.database.port,
    user: config.database.user,
    password: config.database.password,
    database: config.database.database,
    charset: config.database.charset,
    timezone: config.database.timezone,
    supportBigNumbers: config.database.supportBigNumbers,
  });

  const applicationResources: IApplicationResources = {
    databaseConnection: db,
    services: {
      genre: null,
      movie: null,
      administrator: null,
      user: null,
      tag: null,
      episode: null,
      tvShow: null,
    },
  };

  applicationResources.services.genre = new GenreService(applicationResources);
  applicationResources.services.movie = new MovieService(applicationResources);
  applicationResources.services.administrator = new AdministratorService(
    applicationResources
  );
  applicationResources.services.user = new UserService(applicationResources);
  applicationResources.services.tag = new TagService(applicationResources);
  applicationResources.services.episode = new EpisodeService(
    applicationResources
  );
  applicationResources.services.tvShow = new TvShowService(
    applicationResources
  );

  const application: express.Application = express();

  application.use(
    morgan(config.logging.format, {
      stream: fs.createWriteStream(
        config.logging.path + "/" + config.logging.filename,
        { flags: "a" }
      ),
    })
  );

  application.use(cors());
  application.use(express.json());

  application.use(
    config.server.static.route,
    express.static(config.server.static.path, {
      index: config.server.static.index,
      dotfiles: config.server.static.dotfiles,
      cacheControl: config.server.static.cacheControl,
      etag: config.server.static.etag,
      maxAge: config.server.static.maxAge,
    })
  );

  for (const router of config.routers) {
    router.setupRoutes(application, applicationResources);
  }

  application.use((req, res) => {
    res.sendStatus(404);
  });

  application.listen(config.server.port);
}

process.on("uncaughtException", (error) => {
  console.error("ERROR:", error);
});

main();
