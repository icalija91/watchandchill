import React, { useEffect, useState } from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import {ErrorMessage} from '@hookform/error-message';
import * as Yup from 'yup';
import { api } from '../../../api/api';
import IEpisode from '../../../models/IEpisode.model';
import ITag from '../../../models/ITag.model';


export interface IEpisodePageUrlParams extends Record<string, string | undefined>{
    id: string;
    eid: string;
}

export interface IEpisodeProperties {
    tvShowId?: number;
}

export default function AddEditEpisode(props: IEpisodeProperties) {

    const params = useParams<IEpisodePageUrlParams>();

    const tvShowId = props?.tvShowId ?? params.id; 
    const episodeId = params.eid;  
    const isAddMode = !episodeId;

    
    const [episode, setEpisode] = useState<IEpisode>();
    const [tags, setTags] = useState<ITag[]>([]);
    const [selectedTags, setSelectedTags] = useState<ITag[]>([]);
    const [ errorMessage, setErrorMessage ] = useState<string>("");

    const navigate = useNavigate();
    
    // form validation rules 
    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Name is required')
            .min(1)
            .max(128),
        synopsis: Yup.string()
            .required('Synopsis is required')
            .min(4)
            .max(512),
        imageUrl: Yup.string()
            .required('Image URL is required')
            .min(4)
            .max(512),
        number: Yup.number()
            .typeError('Episode number is required')
            .required('Episode number is required') 
            .positive(),
        episodeNumber: Yup.number()
            .typeError('Number of episodes in a season is required')
            .required('Number of episodes in a season is required') 
            .positive(),
        seasonNumber: Yup.number()
            .typeError('Season number is required')
            .required('Season number is required') 
            .positive()
    });

    function loadTags() {
        api("get", "/api/tag", "administrator")
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }

            setTags(res.data);
        });
    }

    // functions to build form returned by useForm() hook
    const { register, handleSubmit, reset, setValue, formState: { errors }, formState } = useForm<IEpisode>({
        resolver: yupResolver(validationSchema)
    });

    const onSubmit =(data: IEpisode) => {
        return isAddMode
            ? addEpisode(data)
            : editEpisode(episodeId, data);
    }

    const resolveCancel =() => {
        return isAddMode
            ? "/tvShows/" + tvShowId
            : "/tvShows/" + tvShowId + "/episodes";
    }

    const addEpisode = (data: IEpisode) => {
        var tagsToSend = selectedTags.map(t => t.tagId);
    api("post", "/api/tvshow/" + tvShowId + "/episode"  , "administrator", { name: data.name, number:data.number,
        synopsis:data.synopsis,imageUrl:data.imageUrl,episodeNumber:data.episodeNumber,seasonNumber:data.seasonNumber,tagIds: tagsToSend})
    .then(res => {
        if (res.status === 'error') {
            return setErrorMessage(res.data + "");
        }
        reset();
        navigate("/tvShows/" + tvShowId + "/episodes");
    });
    }

    function editEpisode(id: any, data: any) {
        var tagsToSend = selectedTags.map(t => t.tagId);
    api("put", "/api/tvshow/"+ tvShowId + "/episode/" + episodeId, "administrator", { name: data.name, number:data.number,
        synopsis:data.synopsis,imageUrl:data.imageUrl,episodeNumber:data.episodeNumber,seasonNumber:data.seasonNumber,tagIds: tagsToSend})
    .then(res => {
        if (res.status === 'error') {
            return setErrorMessage(res.data + "");
        }
        reset();
        navigate("/tvShows/" + tvShowId + "/episodes");
    });
    }

  const handleTagChange = (selectedOptions: HTMLOptionElement[]) => {
    const selectedTags = selectedOptions.map(option => {
        return {
           tagId: parseInt(option.value),
           name: option.text,
        };
      });
      setSelectedTags(selectedTags);
  };

    useEffect(() => {
        if (!isAddMode) {
            api("get", "/api/tvshow/"+ tvShowId + "/episode/" + episodeId, "administrator")
            .then(res => {
                if (res.status === 'error'){
                    throw new Error("Unknown error while loading episode...");
                }
                
                 setEpisode(res.data);

                 setValue('name',episode?.name ?? "");
                 setValue('number',episode?.number ?? 0);
                 setValue('episodeNumber',episode?.episodeNumber ?? 0);
                 setValue('synopsis',episode?.synopsis ?? "");
                 setValue('imageUrl',episode?.imageUrl ?? "");
                 setValue('seasonNumber',episode?.seasonNumber ?? 0);
                 setSelectedTags(episode?.tagIds ?? []);
            })
        }
        loadTags();
    }, [isAddMode, episode?.name, episode?.number, episode?.episodeNumber, episode?.synopsis, episode?.imageUrl, episode?.seasonNumber, episodeId, params.id, setValue]);

    return (
        <div className="row">
        <div className="col-md-4">
         <div className="jumbotron">
            <h1 className="display-4">{isAddMode ? 'Add Episode' : 'Edit Episode'}</h1>
        <form onSubmit={handleSubmit(onSubmit)} >
                                 { errorMessage && <p>Error: { errorMessage}</p> }
            <div className="form-row">
            <div className="form-group col space-top">
                    <label>Name:</label>
                    <input {...register("name")} type="text" className={`form-control ${errors.name ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="name" /></div>
                </div>
               
                <div className="form-group col space-top">
                    <label>Episode number:</label>
                    <input {...register("number")} type="number" className={`form-control ${errors.number ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="number" /></div>
                </div>

                <div className="form-group col space-top">
                    <label>Number of episodes in a season:</label>
                    <input {...register("episodeNumber")} type="number" className={`form-control ${errors.episodeNumber ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="episodeNumber" /></div>
                </div>

                <div className="form-group col space-top">
                    <label>Season number:</label>
                    <input {...register("seasonNumber")} type="number" className={`form-control ${errors.seasonNumber ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="seasonNumber" /></div>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col space-top">
                    <label>Synopsis:</label>
                    <textarea  {...register("synopsis")}  className={`form-control ${errors.synopsis ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="synopsis" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Image link:</label>
                    <input {...register("imageUrl")} type="text"  className={`form-control ${errors.imageUrl ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="imageUrl" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Tags:</label>
                    <select  {...register("tagIds")} 
                    value={selectedTags.map(tag => tag.tagId.toString())}
                    onChange={(e) => handleTagChange(Array.from(e.target.selectedOptions))}
                    className={`form-control ${errors.tagIds ? 'is-invalid' : ''}`} multiple>
                        {tags.map(tag => (
                                <option key ={tag.tagId}  value={tag.tagId} >{tag.name}</option>
                        ) ) }
                    </select>
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="tagIds" /></div>
                </div>
            </div>
            
            <div className="form-group space-top">
                <button type="submit" disabled={formState.isSubmitting} className="btn btn-light px-5 " style={{borderColor:"black", marginRight:10}}>
                    {formState.isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                    SAVE
                </button>
                <Link to={resolveCancel()} className="btn btn-dark px-5 ">CANCEL</Link>
            </div>
        </form>
        </div>
        </ div>
        </div>
    );
}

