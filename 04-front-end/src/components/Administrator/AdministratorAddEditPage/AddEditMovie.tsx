import React, { useEffect, useState } from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import {ErrorMessage} from '@hookform/error-message';
import * as Yup from 'yup';
import { api } from '../../../api/api';
import IMovie from '../../../models/IMovie.model';
import ITag from '../../../models/ITag.model';
import IGenre from '../../../models/IGenre.model';


export interface IMoviePageUrlParams extends Record<string, string | undefined>{
    id: string;
}

export interface IMovieProperties {
    movieId?: number;
}

export default function AddEditMovie(props: IMovieProperties) {

    const params = useParams<IMoviePageUrlParams>();

    const movieId = props?.movieId ?? params.id;    
    const isAddMode = !movieId;

    
    const [movie, setMovie] = useState<IMovie>();
    const [tags, setTags] = useState<ITag[]>([]);
    const [selectedTags, setSelectedTags] = useState<ITag[]>([]);
    const [genres, setGenres] = useState<IGenre[]>([]);
    const [selectedGenres, setSelectedGenres] = useState<IGenre[]>([]);
    const [ errorMessage, setErrorMessage ] = useState<string>("");

    const navigate = useNavigate();
    
    // form validation rules 
    const validationSchema = Yup.object().shape({
        title: Yup.string()
            .required('Title is required')
            .min(1)
            .max(128),
        serbianTitle: Yup.string()
            .required('Serbian Title is required')
            .min(1)
            .max(128),
        director: Yup.string()
            .required('Director is required')
            .min(2)
            .max(32),
        synopsis: Yup.string()
            .required('Synopsis is required')
            .min(4)
            .max(512),
        imageUrl: Yup.string()
            .required('Image URL is required')
            .min(4)
            .max(512),
        category: Yup.string()
            .required('Category is required'),
        genreIds: Yup.array()
            .min(1,'You must select at least one genre')       
    });

    function loadTags() {
        api("get", "/api/tag", "user")
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }

            setTags(res.data);
        });
    }

    function loadGenres() {
        api("get", "/api/genre", "user")
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }

            setGenres(res.data);
        });
    }

    const resolveCancel =() => {
        return isAddMode
            ? "/administrator/dashboard"
            : "/movies";
    }

    // functions to build form returned by useForm() hook
    const { register, handleSubmit, reset, setValue, formState: { errors }, formState } = useForm<IMovie>({
        resolver: yupResolver(validationSchema)
    });

    const onSubmit =(data: IMovie) => {
        return isAddMode
            ? addMovie(data)
            : editMovie(movieId, data);
    }

    const addMovie = (data: IMovie) => {
        var tagsToSend = selectedTags.map(t => t.tagId);
        var genreToSend = selectedGenres.map(g => g.genreId);
    api("post", "/api/movie/"  , "administrator", { title: data.title, serbianTitle:data.serbianTitle,director:data.director,
        synopsis:data.synopsis,imageUrl:data.imageUrl,category:data.category, genreIds:genreToSend, tagIds: tagsToSend})
    .then(res => {
        if (res.status === 'error') {
            return setErrorMessage(res.data + "");
        }
        reset();
        navigate("/movies");
    });
    }

    function editMovie(id: any, data: any) {
        var tagsToSend = selectedTags.map(t => t.tagId);
        var genreToSend = selectedGenres.map(g => g.genreId);
    api("put", "/api/movie/"+ movieId  , "administrator", { title: data.title, serbianTitle:data.serbianTitle,director:data.director,
        synopsis:data.synopsis,imageUrl:data.imageUrl,category:data.category, genreIds:genreToSend, tagIds: tagsToSend})
    .then(res => {
        if (res.status === 'error') {
            return setErrorMessage(res.data + "");
        }
        reset();
        navigate("/movies");
    });
    }

  const handleTagChange = (selectedOptions: HTMLOptionElement[]) => {
    const selectedTags = selectedOptions.map(option => {
        return {
           tagId: parseInt(option.value),
           name: option.text,
        };
      });
      setSelectedTags(selectedTags);
  };

  const handleGenreChange = (selectedOptions: HTMLOptionElement[]) => {
    const selectedGenres = selectedOptions.map(option => {
        return {
           genreId: parseInt(option.value),
           name: option.text,
        };
      });
      setSelectedGenres(selectedGenres);
  };

    useEffect(() => {
        if (!isAddMode) {
            api("get", "/api/movie/" + movieId, "user")
            .then(res => {
                if (res.status === 'error'){
                    throw new Error("Unknown error while loading movie...");
                }
                
                 setMovie(res.data);

                 setValue('title',movie?.title ?? "");
                 setValue('serbianTitle',movie?.serbianTitle ?? "");
                 setValue('director',movie?.director ?? "");
                 setValue('synopsis',movie?.synopsis ?? "");
                 setValue('imageUrl',movie?.imageUrl ?? "");
                 setValue('category',movie?.category ?? "indie");
                 setSelectedTags(movie?.tagIds ?? []);
                 setSelectedGenres(movie?.genreIds ?? []);
            })
        }
        loadTags();
        loadGenres();
    }, [isAddMode, movie?.category, movie?.director, movie?.imageUrl, movie?.serbianTitle, movie?.synopsis, movie?.title, movieId, params.id, setValue]);

    return (
        <div className="row">
        <div className="col-md-4">
         <div className="jumbotron">
            <h1 className="display-4">{isAddMode ? 'Add Movie' : 'Edit Movie'}</h1>
        <form onSubmit={handleSubmit(onSubmit)} >
        { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
            <div className="form-row">
            <div className="form-group col space-top">
                    <label>Title:</label>
                    <input {...register("title")} type="text" className={`form-control ${errors.title ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="title" /></div>
                </div>
               
                <div className="form-group col space-top">
                    <label>Serbian Title:</label>
                    <input {...register("serbianTitle")} type="text" className={`form-control ${errors.serbianTitle ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="serbianTitle" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Director:</label>
                    <input {...register("director")} type="text"  className={`form-control ${errors.director ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="director" /></div>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col space-top">
                    <label>Synopsis:</label>
                    <textarea  {...register("synopsis")}  className={`form-control ${errors.synopsis ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="synopsis" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Image link:</label>
                    <input {...register("imageUrl")} type="text"  className={`form-control ${errors.imageUrl ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="imageUrl" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Category:</label>
                    <select  {...register("category")}  className={`form-control ${errors.category ? 'is-invalid' : ''}`}>
                        <option value=""></option>
                        <option value="artistic">artistic</option>
                        <option value="indie">indie</option>
                        <option value="box-office">box-office</option>
                        <option value="documentary">documentary</option>
                        <option value="silent">silent</option>
                    </select>
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="category" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Tags:</label>
                    <select  {...register("tagIds")} 
                    value={selectedTags.map(tag => tag.tagId.toString())}
                    onChange={(e) => handleTagChange(Array.from(e.target.selectedOptions))}
                    className={`form-control ${errors.tagIds ? 'is-invalid' : ''}`} multiple>
                        {tags.map(tag => (
                                <option key ={tag.tagId}  value={tag.tagId} >{tag.name}</option>
                        ) ) }
                    </select>
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="tagIds" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Genre:</label>
                    <select  {...register("genreIds")} 
                    value={selectedGenres.map(genre => genre.genreId.toString())}
                    onChange={(e) => handleGenreChange(Array.from(e.target.selectedOptions))}
                     className={`form-control ${errors.genreIds ? 'is-invalid' : ''}`} multiple>
                        {genres.map(genre => (
                                <option key ={genre.genreId}  value={genre.genreId} >{genre.name}</option>
                        ) ) }
                    </select>
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="genreIds" /></div>
                </div>
            </div>
            
            <div className="form-group space-top">
                <button type="submit" disabled={formState.isSubmitting} className="btn btn-light px-5 " style={{borderColor:"black", marginRight:10}}>
                    {formState.isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                    SAVE
                </button>
                <Link to={resolveCancel()} className="btn btn-dark px-5 ">CANCEL</Link>
            </div>
        </form>
        </div>
        </ div>
        </div>
    );
}

