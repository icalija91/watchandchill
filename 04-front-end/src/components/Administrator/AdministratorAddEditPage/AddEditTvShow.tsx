import React, { useEffect, useState } from 'react';
import { Link, useParams, useNavigate } from 'react-router-dom';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import {ErrorMessage} from '@hookform/error-message';
import * as Yup from 'yup';
import { api } from '../../../api/api';
import ITvShow from '../../../models/ITvShow.model';
import ITag from '../../../models/ITag.model';


export interface ITvShowPageUrlParams extends Record<string, string | undefined>{
    id: string;
}

export interface ITvShowProperties {
    tvShowId?: number;
}

export default function AddEditTvShow(props: ITvShowProperties) {

    const params = useParams<ITvShowPageUrlParams>();

    const tvShowId = props?.tvShowId ?? params.id;    
    const isAddMode = !tvShowId;

    
    const [tvShow, setTvShow] = useState<ITvShow>();
    const [tags, setTags] = useState<ITag[]>([]);
    const [selectedTags, setSelectedTags] = useState<ITag[]>([]);
    const [ errorMessage, setErrorMessage ] = useState<string>("");

    const navigate = useNavigate();
    
    // form validation rules 
    const validationSchema = Yup.object().shape({
        title: Yup.string()
            .required('Title is required')
            .min(1)
            .max(128),
        serbianTitle: Yup.string()
            .required('Serbian Title is required')
            .min(1)
            .max(128),
        director: Yup.string()
            .required('Director is required')
            .min(2)
            .max(32),
        synopsis: Yup.string()
            .required('Synopsis is required')
            .min(4)
            .max(512),
        imageUrl: Yup.string()
            .required('Image URL is required')
            .min(4)
            .max(512),
        category: Yup.string()
            .required('Category is required'),   
    });

    function loadTags() {
        api("get", "/api/tag", "user")
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }

            setTags(res.data);
        });
    }

    // functions to build form returned by useForm() hook
    const { register, handleSubmit, reset, setValue, formState: { errors }, formState } = useForm<ITvShow>({
        resolver: yupResolver(validationSchema)
    });

    const onSubmit =(data: ITvShow) => {
        return isAddMode
            ? addTvShow(data)
            : editTvShow(tvShowId, data);
    }

    const resolveCancel =() => {
        return isAddMode
            ? "/administrator/dashboard"
            : "/tvShows";
    }

    const addTvShow = (data: ITvShow) => {
        var tagsToSend = selectedTags.map(t => t.tagId);
    api("post", "/api/tvshow/"  , "administrator", { title: data.title, serbianTitle:data.serbianTitle,director:data.director,
        synopsis:data.synopsis,imageUrl:data.imageUrl,category:data.category, tagIds: tagsToSend})
    .then(res => {
        if (res.status === 'error') {
            return setErrorMessage(res.data + "");
        }
        reset();
        navigate("/tvShows");
    });
    }

    function editTvShow(id: any, data: any) {
        var tagsToSend = selectedTags.map(t => t.tagId);
    api("put", "/api/tvshow/"+ tvShowId  , "administrator", { title: data.title, serbianTitle:data.serbianTitle,director:data.director,
        synopsis:data.synopsis,imageUrl:data.imageUrl,category:data.category, tagIds: tagsToSend})
    .then(res => {
        if (res.status === 'error') {
            return setErrorMessage(res.data + "");
        }
        reset();
        navigate("/tvShows");
    });
    }

  const handleTagChange = (selectedOptions: HTMLOptionElement[]) => {
    const selectedTags = selectedOptions.map(option => {
        return {
           tagId: parseInt(option.value),
           name: option.text,
        };
      });
      setSelectedTags(selectedTags);
  };

    useEffect(() => {
        if (!isAddMode) {
            api("get", "/api/tvshow/" + tvShowId, "user")
            .then(res => {
                if (res.status === 'error'){
                    throw new Error("Unknown error while loading tv show...");
                }
                
                 setTvShow(res.data);

                 setValue('title',tvShow?.title ?? "");
                 setValue('serbianTitle',tvShow?.serbianTitle ?? "");
                 setValue('director',tvShow?.director ?? "");
                 setValue('synopsis',tvShow?.synopsis ?? "");
                 setValue('imageUrl',tvShow?.imageUrl ?? "");
                 setValue('category',tvShow?.category ?? "indie");
                 setSelectedTags(tvShow?.tagIds ?? []);
            })
        }
        loadTags();
    }, [isAddMode, tvShow?.category, tvShow?.director, tvShow?.imageUrl, tvShow?.serbianTitle, tvShow?.synopsis, tvShow?.title, tvShowId, params.id, setValue]);

    return (
        <div className="row">
        <div className="col-md-4">
         <div className="jumbotron">
            <h1 className="display-4">{isAddMode ? 'Add Tv Show' : 'Edit Tv Show'}</h1>
        <form onSubmit={handleSubmit(onSubmit)} >
                                 { errorMessage && <p>Error: { errorMessage}</p> }
            <div className="form-row">
            <div className="form-group col space-top">
                    <label>Title:</label>
                    <input {...register("title")} type="text" className={`form-control ${errors.title ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="title" /></div>
                </div>
               
                <div className="form-group col space-top">
                    <label>Serbian Title:</label>
                    <input {...register("serbianTitle")} type="text" className={`form-control ${errors.serbianTitle ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="serbianTitle" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Director:</label>
                    <input {...register("director")} type="text"  className={`form-control ${errors.director ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="director" /></div>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col space-top">
                    <label>Synopsis:</label>
                    <textarea  {...register("synopsis")}  className={`form-control ${errors.synopsis ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="synopsis" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Image link:</label>
                    <input {...register("imageUrl")} type="text"  className={`form-control ${errors.imageUrl ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="imageUrl" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Category:</label>
                    <select  {...register("category")}  className={`form-control ${errors.category ? 'is-invalid' : ''}`}>
                        <option value=""></option>
                        <option value="artistic">artistic</option>
                        <option value="indie">indie</option>
                        <option value="box-office">box-office</option>
                        <option value="documentary">documentary</option>
                        <option value="silent">silent</option>
                    </select>
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="category" /></div>
                </div>
                <div className="form-group col space-top">
                    <label>Tags:</label>
                    <select  {...register("tagIds")} 
                    value={selectedTags.map(tag => tag.tagId.toString())}
                    onChange={(e) => handleTagChange(Array.from(e.target.selectedOptions))}
                    className={`form-control ${errors.tagIds ? 'is-invalid' : ''}`} multiple>
                        {tags.map(tag => (
                                <option key ={tag.tagId}  value={tag.tagId} >{tag.name}</option>
                        ) ) }
                    </select>
                    <div className="invalid-feedback"><ErrorMessage errors={errors} name="tagIds" /></div>
                </div>
            </div>
            
            <div className="form-group space-top">
                <button type="submit" disabled={formState.isSubmitting} className="btn btn-light px-5 " style={{borderColor:"black", marginRight:10}}>
                    {formState.isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                    SAVE
                </button>
                <Link to={resolveCancel()} className="btn btn-dark px-5 ">CANCEL</Link>
            </div>
        </form>
        </div>
        </ div>
        </div>
    );
}

