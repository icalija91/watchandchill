import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { api } from "../../../api/api";
import AppStore from "../../../stores/AppStore";

export default function AdministratorLoginPage(){

    const [ username, setUsername ] = useState<string>("");
    const [ password, setPassword ] = useState<string>("");
    const [ error, setError ] = useState<string>("");

    const navigate = useNavigate();

    const doLogin = () => {
        api("post", "/api/auth/administrator/login", "administrator", { username, password })
        .then(res => {
            if (res.status !== "ok") {
                throw new Error("Could not log in. Reason: " + JSON.stringify(res.data));
            }

            return res.data;
        })
        .then(data => {
            AppStore.dispatch( { type: "auth.update", key: "authToken", value: data?.authToken } );
            AppStore.dispatch( { type: "auth.update", key: "refreshToken", value: data?.refreshToken } );
            AppStore.dispatch( { type: "auth.update", key: "identity", value: username } );
            AppStore.dispatch( { type: "auth.update", key: "id", value: +(data?.id) } );
            AppStore.dispatch( { type: "auth.update", key: "role", value: "administrator" } );

            navigate("/administrator/dashboard", {
                replace: true,
            });
        })
        .catch(error => {
            setError(error?.message ?? "Could not log in!");

            setTimeout(() => {
                setError("");
            }, 3500);
        });
    };
    
    return (
        <div className="div-center">
                <h1 className="h5 mb-3">Log into your administrator account</h1>

                <div className="form-group mb-3">
                    <div className="input-group">
                        <input className="form-control" type="text" placeholder="Enter your username" value={ username }
                            onChange={ e => setUsername(e.target.value)}/>
                    </div>
                </div>

                <div className="form-group mb-3">
                    <div className="input-group">
                        <input className="form-control" type="password" placeholder="Enter your password" value={ password }
                             onChange={ e => setPassword(e.target.value)}/>
                    </div>
                </div>

                <div className="form-group mb-3">
                    <button className="btn btn-dark px-5 main-btn" onClick= { () =>  doLogin() }>
                        LOGIN
                    </button>
                </div>
                { error && <div className="mt-3 alert alert-danger" >Error: { error}</div> }

        </div>

    );
}