import React from "react";
import { Link } from "react-router-dom";

export default function AdministratorDashboard() {
    return (
        <div className="card-group">
                <div className="card" style={{margin:20}}>
                    <div className="card-body">
                        <div className="card-title">
                            <h2 className="h5">Movies</h2>
                        </div>
                        <div className="card-footer">
                            <Link className="btn btn-light px-5 main-btn" style={{borderColor:"black", marginBottom:10}}  to="/movies/add">ADD</Link>
                            <Link className="btn btn-dark px-5 main-btn"  to="/movies">LIST ALL</Link>
                        </div>
                    </div>
                </div>

                <div className="card" style={{margin:20}}>
                    <div className="card-body">
                        <div className="card-title">
                            <h2 className="h5">TV Shows</h2>
                        </div>
                        <div className="card-footer">
                            <Link className="btn btn-light px-5 main-btn" style={{borderColor:"black", marginBottom:10}}  to="/tvShows/add">ADD</Link>
                            <Link className="btn btn-dark px-5 main-btn" to="/tvShows">LIST ALL</Link>
                        </div>
                    </div>
                </div>

                <div className="card" style={{margin:20}}>
                    <div className="card-body">
                        <div className="card-title">
                            <h2 className="h5">Tags</h2>
                        </div>
                        <div className="card-footer">
                            <Link className="btn btn-dark px-5 main-btn"  to="/administrator/tags">LIST ALL</Link>
                        </div>
                    </div>
                </div>

                <div className="card" style={{margin:20}}>
                    <div className="card-body">
                        <div className="card-title">
                            <h2 className="h5">Genres</h2>
                        </div>
                        <div className="card-footer">
                            <Link className="btn btn-dark px-5 main-btn"  to="/administrator/genres">LIST ALL</Link>
                        </div>
                    </div>
                </div>

                <div className="card" style={{margin:20}}>
                    <div className="card-body">
                        <div className="card-title">
                            <h2 className="h5">Administrators</h2>
                        </div>
                        <div className="card-footer">
                            <Link className="btn btn-dark px-5 main-btn"  to="/administrator/administrators">LIST ALL</Link>
                        </div>
                    </div>
                </div>

                <div className="card" style={{margin:20}}>
                    <div className="card-body">
                        <div className="card-title">
                            <h2 className="h5">Users</h2>
                        </div>
                        <div className="card-footer">
                            <Link className="btn btn-dark px-5 main-btn"  to="/administrator/users">LIST ALL</Link>
                        </div>
                    </div>
                </div>

            </div>
    );
}