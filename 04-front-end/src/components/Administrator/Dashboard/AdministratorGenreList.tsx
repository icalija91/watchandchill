import React, { useEffect, useState } from "react";
import { api } from "../../../api/api";
import IGenre from '../../../models/IGenre.model';
import ConfirmAction from "../../../helpers/ConfirmAction";

interface IAdministratorGenreListRowProperties {
    genre: IGenre,
}

export default function AdministratorGenreList() {
    const [ Genres, setGenres ] = useState<IGenre[]>([]);
    const [ errorMessage, setErrorMessage ] = useState<string>("");
    const [ showAddNewGenre, setShowAddNewGenre ] = useState<boolean>(false);

    function AdministratorGenreListRow(props: IAdministratorGenreListRowProperties) {
        const [ name, setName ] = useState<string>(props.genre.name);
        const [ deleteRequested, setDeleteRequested ] = useState<boolean>(false);


        const nameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
            setName( e.target.value );
        }

        const doEditGenre = (e: any) => {
            api("put", "/api/genre/" + props.genre.genreId, "administrator", { name })
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage("Could not edit this genre!");
                }

                loadGenres();
            })
        }
        const doDeleteGenre = () => {
            api("delete", "/api/genre/" + props.genre.genreId, "administrator")
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage("Could not delete this genre!");
                }

            loadGenres();
            })
        }

        return (
            <tr>
                <td>{ props.genre.genreId }</td>
                <td>
                    <div className="input-group">
                        <input className="form-control form-control-sm"
                               type="text"
                               onChange={ e => nameChanged(e) }
                               value={ name } />
                        { props.genre.name !== name
                            ? <button className="btn btn-secondary btn-sm" onClick={ e => doEditGenre(e) }>
                                  Save
                              </button>
                            : ''
                        }
                    </div>
                </td>
                <td>
                <button className="btn btn-danger btn-sm" onClick={ () => setDeleteRequested(true) }>
                        Delete
                    </button>

                    { deleteRequested && <ConfirmAction
                        title="Confirm that you want to delete this genre"
                        message={ "Are you sure that you want to delete this genre: \"" + props.genre.name + "\"?" }
                        onNo={ () => setDeleteRequested(false) }
                        onYes={ () => doDeleteGenre() }
                    /> }
                </td>
            </tr>
        );
    }

    function AdministratorGenreListAddRow() {
        const [ name, setName ] = useState<string>("");

        const nameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
            setName( e.target.value );
        }

        const doAddGenre = (e: any) => {
            api("post", "/api/genre", "administrator", { name })
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage("Could not add this genre!");
                }

                loadGenres();

                setName("");
                setShowAddNewGenre(false);
            });
        }

        return (
            <tr>
                <td> </td>
                <td>
                    <div className="input-group">
                        <input className="form-control form-control-sm"
                               type="text"
                               onChange={ e => nameChanged(e) }
                               value={ name } />
                        { name.trim().length >= 4 && name.trim().length <= 32
                            ? <button className="btn btn-secondary btn-sm" onClick={ e => doAddGenre(e) }>
                                  Save
                              </button>
                            : ''
                        }
                    </div>
                </td>
                <td>
                    <button className="btn btn-danger btn-sm" onClick={ () => {
                        setShowAddNewGenre(false);
                        setName("");
                    } }>
                        Cancel
                    </button>
                </td>
            </tr>
        );
    }

    const loadGenres = () => {
        api("get", "/api/genre", "administrator")
        .then(apiResponse => {
            if (apiResponse.status === 'ok') {
                return setGenres(apiResponse.data);
            }

            throw new Error('Unknown error while loading genres...');
        })
        .catch(error => {
            setErrorMessage(error?.message ?? 'Unknown error while loading genres...');
        });
    }

    useEffect(() => {
        loadGenres();
    }, [ ]);

    return (
        <div>
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
            { !errorMessage &&
                <div>
                    <button className="btn float-end btn-light px-5 " style={{borderColor:"black",margin:10}} onClick={() => setShowAddNewGenre(true)}>ADD GENRE</button>
                    
                    <table className="table table-dark table-bordered table-striped table-hover table-sm mt-3">
                        <thead className="thead-dark">
                            <tr>
                                <th className="size-row-id">ID</th>
                                <th>Name</th>
                                <th className="size-row-options">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            { showAddNewGenre && <AdministratorGenreListAddRow /> }
                            { Genres.map(Genre => <AdministratorGenreListRow key={ "size-row-" + Genre.genreId } genre={ Genre } />) }
                        </tbody>
                    </table>
                </div>
            }
        </div>
    );
}
