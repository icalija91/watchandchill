import { useEffect, useState } from "react";
import { api } from "../../../api/api";
import IAdministrator from "../../../models/IAdministrator.model";
import ConfirmAction from "../../../helpers/ConfirmAction";
import { Link } from "react-router-dom";

interface IAdministratorRowProperties {
    administrator: IAdministrator;
}

export default function AdministratorList() {
    const [ administrators, setAdministrators ] = useState<IAdministrator[]>([]);
    const [ errorMessage, setErrorMessage ] = useState<string>("");

    function loadAdministrators() {
        api("get", "/api/administrator", "administrator")
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }

            setAdministrators(res.data);
        });
    }

    useEffect(loadAdministrators, [ ]);

    function AdminAdministratorRow(props: IAdministratorRowProperties) {
        const [ editPasswordVisible, setEditPasswordVisible ] = useState<boolean>(false);
        const [ newPassword, setNewPassword ] = useState<string>("");
        const [ deleteRequested, setDeleteRequested ] = useState<boolean>(false);

        const doDeleteAdministrator = () => {
            api("delete", "/api/administrator/" + props.administrator.administratorId, "administrator")
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage("Could not delete this administrator!");
                }

            loadAdministrators();
            })
        }

        function doChangePassword() {
            api("put", "/api/administrator/" + props.administrator.administratorId, "administrator", {
                password: newPassword,
            })
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage(res.data + "");
                }

                loadAdministrators();
            });
        }

        function changePassword(e: React.ChangeEvent<HTMLInputElement>) {
            setNewPassword(e.target.value);
        }

        return (
            <tr>
                <td>{ props.administrator.administratorId }</td>
                <td>{ props.administrator.username }</td>
                <td>
                    { !editPasswordVisible && <button className="btn btn-light btn-sm" onClick={() => { setEditPasswordVisible(true); }}>Change password</button> }
                    { editPasswordVisible && <div className="input-group  w-50">
                        <input type="password" className="form-control form-control-sm" value={ newPassword } onChange={ e => changePassword(e) } />
                        <button className="btn btn-light btn-sm" style={{borderColor:"black"}} onClick={() => doChangePassword()}>Save</button>
                        <button className="btn btn-danger btn-sm" onClick={() => { setEditPasswordVisible(false); setNewPassword(""); }}>Cancel</button>
                    </div> }
                    <button className="btn btn-danger btn-sm" style={{marginLeft:20}} onClick={ () => setDeleteRequested(true) }>
                        Delete
                    </button>

                    { deleteRequested && <ConfirmAction
                        title="Confirm that you want to delete this administrator"
                        message={ "Are you sure that you want to delete this administrator: \"" + props.administrator.username + "\"?" }
                        onNo={ () => setDeleteRequested(false) }
                        onYes={ () => doDeleteAdministrator() }
                    /> }
                </td>
            </tr>
        );
    }

    return (
        <div>
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
            { !errorMessage &&
            <div>
            <Link className="btn float-end btn-light  " style={{borderColor:"black",margin:10}} to="/administrator/add">ADD ADMINISTRATOR</Link>
            <table className="table table-dark table-bordered table-striped table-hover table-sm mt-3">
                    <thead>
                        <tr>
                            <th className="size-row-id">ID</th>
                            <th>Username</th>
                            <th className="size-row-options">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        { administrators.map(admin => <AdminAdministratorRow key={ "administrator" + admin.administratorId } administrator={ admin } />) }
                    </tbody>
                </table>
                </div>
            }
        </div>
    );
}
