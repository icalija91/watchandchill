import React, { useEffect, useState } from "react";
import { api } from "../../../api/api";
import ITag from '../../../models/ITag.model';
import ConfirmAction from "../../../helpers/ConfirmAction";

interface IAdministratorTagListRowProperties {
    tag: ITag,
}

export default function AdministratorTagList() {
    const [ tags, setTags ] = useState<ITag[]>([]);
    const [ errorMessage, setErrorMessage ] = useState<string>("");
    const [ showAddNewTag, setShowAddNewTag ] = useState<boolean>(false);

    function AdministratorTagListRow(props: IAdministratorTagListRowProperties) {
        const [ name, setName ] = useState<string>(props.tag.name);
        const [ deleteRequested, setDeleteRequested ] = useState<boolean>(false);


        const nameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
            setName( e.target.value );
        }

        const doEditTag = (e: any) => {
            api("put", "/api/tag/" + props.tag.tagId, "administrator", { name })
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage("Could not edit this tag!");
                }

                loadTags();
            })
        }
        const doDeleteTag = () => {
            api("delete", "/api/tag/" + props.tag.tagId, "administrator")
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage("Could not delete this tag!");
                }

            loadTags();
            })
        }

        return (
            <tr>
                <td>{ props.tag.tagId }</td>
                <td>
                    <div className="input-group">
                        <input className="form-control form-control-sm"
                               type="text"
                               onChange={ e => nameChanged(e) }
                               value={ name } />
                        { props.tag.name !== name
                            ? <button className="btn btn-secondary btn-sm" onClick={ e => doEditTag(e) }>
                                  Save
                              </button>
                            : ''
                        }
                    </div>
                </td>
                <td>
                <button className="btn btn-danger btn-sm" onClick={ () => setDeleteRequested(true) }>
                        Delete
                    </button>

                    { deleteRequested && <ConfirmAction
                        title="Confirm that you want to delete this tag"
                        message={ "Are you sure that you want to delete this tag: \"" + props.tag.name + "\"?" }
                        onNo={ () => setDeleteRequested(false) }
                        onYes={ () => doDeleteTag() }
                    /> }
                </td>
            </tr>
        );
    }

    function AdministratorTagListAddRow() {
        const [ name, setName ] = useState<string>("");

        const nameChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
            setName( e.target.value );
        }

        const doAddTag = (e: any) => {
            api("post", "/api/tag", "administrator", { name })
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage("Could not add this tag!");
                }

                loadTags();

                setName("");
                setShowAddNewTag(false);
            });
        }

        return (
            <tr>
                <td> </td>
                <td>
                    <div className="input-group">
                        <input className="form-control form-control-sm"
                               type="text"
                               onChange={ e => nameChanged(e) }
                               value={ name } />
                        { name.trim().length >= 4 && name.trim().length <= 32
                            ? <button className="btn btn-secondary btn-sm" onClick={ e => doAddTag(e) }>
                                  Save
                              </button>
                            : ''
                        }
                    </div>
                </td>
                <td>
                    <button className="btn btn-danger btn-sm" onClick={ () => {
                        setShowAddNewTag(false);
                        setName("");
                    } }>
                        Cancel
                    </button>
                </td>
            </tr>
        );
    }

    const loadTags = () => {
        api("get", "/api/tag", "administrator")
        .then(apiResponse => {
            if (apiResponse.status === 'ok') {
                return setTags(apiResponse.data);
            }

            throw new Error('Unknown error while loading tags...');
        })
        .catch(error => {
            setErrorMessage(error?.message ?? 'Unknown error while loading tags...');
        });
    }

    useEffect(() => {
        loadTags();
    }, [ ]);

    return (
        <div>
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
            { !errorMessage &&
                <div>
                    <button className="btn float-end btn-light px-5 " style={{borderColor:"black",margin:10}} onClick={() => setShowAddNewTag(true)}>ADD TAG</button>
                    
                    <table className="table table-dark table-bordered table-striped table-hover table-sm mt-3">
                        <thead className="thead-dark">
                            <tr>
                                <th className="size-row-id">ID</th>
                                <th>Name</th>
                                <th className="size-row-options">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            { showAddNewTag && <AdministratorTagListAddRow /> }
                            { tags.map(tag => <AdministratorTagListRow key={ "size-row-" + tag.tagId } tag={ tag } />) }
                        </tbody>
                    </table>
                </div>
            }
        </div>
    );
}
