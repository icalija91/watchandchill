import { useEffect, useState } from "react";
import { api } from "../../../api/api";
import IUser from "../../../models/IUser.model";
import ConfirmAction from "../../../helpers/ConfirmAction";

interface IUserRowProperties {
    user: IUser;
}

export default function AdministratorUserList() {
    const [ users, setUsers ] = useState<IUser[]>([]);
    const [ errorMessage, setErrorMessage ] = useState<string>("");

    function loadUsers() {
        api("get", "/api/user", "user")
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }

            setUsers(res.data);
        });
    }

    useEffect(loadUsers, [ ]);

    function UserRow(props: IUserRowProperties) {
        const [ editPasswordVisible, setEditPasswordVisible ] = useState<boolean>(false);
        const [ newPassword, setNewPassword ] = useState<string>("");
        const [ deleteRequested, setDeleteRequested ] = useState<boolean>(false);

        const doDeleteUser = () => {
            api("delete", "/api/user/" + props.user.userId, "user")
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage("Could not delete this user!");
                }

            loadUsers();
            })
        }

        function doChangePassword() {
            api("put", "/api/user/" + props.user.userId, "user", {
                password: newPassword,
            })
            .then(res => {
                if (res.status === 'error') {
                    return setErrorMessage(res.data + "");
                }

                loadUsers();
            });
        }

        function changePassword(e: React.ChangeEvent<HTMLInputElement>) {
            setNewPassword(e.target.value);
        }

        return (
            <tr>
                <td>{ props.user.userId }</td>
                <td>{ props.user.username }</td>
                <td>{ props.user.email }</td>
                <td>
                    { !editPasswordVisible && <button className="btn btn-light btn-sm" onClick={() => { setEditPasswordVisible(true); }}>Change password</button> }
                    { editPasswordVisible && <div className="input-group  w-50">
                        <input type="password" className="form-control form-control-sm" value={ newPassword } onChange={ e => changePassword(e) } />
                        <button className="btn btn-light btn-sm" style={{borderColor:"black"}} onClick={() => doChangePassword()}>Save</button>
                        <button className="btn btn-danger btn-sm" onClick={() => { setEditPasswordVisible(false); setNewPassword(""); }}>Cancel</button>
                    </div> }
                    <button className="btn btn-danger btn-sm" style={{marginLeft:20}} onClick={ () => setDeleteRequested(true) }>
                        Delete
                    </button>

                    { deleteRequested && <ConfirmAction
                        title="Confirm that you want to delete this user"
                        message={ "Are you sure that you want to delete this user: \"" + props.user.username + "\"?" }
                        onNo={ () => setDeleteRequested(false) }
                        onYes={ () => doDeleteUser() }
                    /> }
                </td>
            </tr>
        );
    }

    return (
        <div>
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
            { !errorMessage &&
            <div>
            <table className="table table-dark table-bordered table-striped table-hover table-sm mt-3">
                    <thead>
                        <tr>
                            <th className="size-row-id">ID</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th className="size-row-options">Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        { users.map(user => <UserRow key={ "User" + user.userId } user={ user } />) }
                    </tbody>
                </table>
                </div>
            }
        </div>
    );
}
