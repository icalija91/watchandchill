import './Application.sass';
import { Container } from 'react-bootstrap';
import UserLoginPage from '../User/UserLoginPage/UserLoginPage';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import TvShowList from '../Pages/TvShow/TvShowList';
import TvShowPage from '../Pages/TvShow/TvShowPage';
import Menu from '../Menu/Menu';
import AdministratorLoginPage from '../Administrator/AdministratorLoginPage/AdministratorLoginPage';
import AdministratorDashboard from '../Administrator/Dashboard/AdministratorDashboard';
import AdministratorTagList from '../Administrator/Dashboard/AdministratorTagList';
import AdministratorGenreList from '../Administrator/Dashboard/AdministratorGenreList';
import MovieList from '../Pages/Movie/MovieList';
import MoviePage from '../Pages/Movie/MoviePage';
import AdministratorList from '../Administrator/Dashboard/AdministratorList';
import AdministratorAdd from '../Administrator/Dashboard/AdministratorAdd';
import UserRegisterPage from '../User/UserRegisterPage/UserRegisterPage';
import AdministratorUserList from '../Administrator/Dashboard/AdministratorUserList';
import UserPasswordChanger from '../User/UserPasswordChanger/UserPasswordChanger';
import EpisodeList from '../Pages/TvShow/Episode/EpisodeList';
import EpisodePage from '../Pages/TvShow/Episode/EpisodePage';
import AddEditMovie from '../Administrator/AdministratorAddEditPage/AddEditMovie';
import AddEditTvShow from '../Administrator/AdministratorAddEditPage/AddEditTvShow';
import AddEditEpisode from '../Administrator/AdministratorAddEditPage/AddEditEpisode';
import UserHomepage from '../User/Homepage/UserHomepage';

function Application() {
  return (
    <Container fluid className="app-main">

      <Router>
      <Menu></Menu>

        <Routes>
          <Route path='/' element={ <UserHomepage/> } />
          <Route path='/auth/user/login' element={ <UserLoginPage/>} />
          <Route path='/auth/user/register' element={ <UserRegisterPage/>} />
          <Route path='/auth/administrator/login' element={ <AdministratorLoginPage/>} />
          <Route path="/administrator/dashboard" element={ <AdministratorDashboard /> } />
          <Route path="/administrator/tags" element={ <AdministratorTagList /> } />
          <Route path="/administrator/genres" element={ <AdministratorGenreList /> } />
          <Route path="/administrator/administrators" element={ <AdministratorList /> } />
          <Route path="/administrator/users" element={ <AdministratorUserList /> } />
          <Route path="/administrator/add" element={ <AdministratorAdd /> } />
          <Route path="/user/:id/password" element={ <UserPasswordChanger/> } />
          <Route path='/movies' element={ <MovieList/> } />
          <Route path='/movies/:id' element={ <MoviePage/> } />
          <Route path='/movies/add' element={ <AddEditMovie/> } />
          <Route path='/movies/edit/:id' element={ <AddEditMovie/> } />
          <Route path='/tvShows' element={ <TvShowList/> } />
          <Route path='/tvShows/:id' element={ <TvShowPage/> } />
          <Route path='/tvShows/:id/episodes' element={ <EpisodeList/> } />
          <Route path='/tvShows/:id/episode/:eid' element={ <EpisodePage/> } />
          <Route path='/tvShows/add' element={ <AddEditTvShow/> } />
          <Route path='/tvShows/edit/:id' element={ <AddEditTvShow/> } />
          <Route path='/tvShows/:id/episode/add' element={ <AddEditEpisode/> } />
          <Route path='/tvShows/:id/episode/edit/:eid' element={ <AddEditEpisode/> } />
        </Routes>
      </Router>
    </Container>
  );
}

export default Application;
