import React, { useState } from "react";
import AppStore from "../../stores/AppStore";
import MenuVisitor from "./MenuVisitor";
import MenuAdministrator from "./MenuAdministrator";
import MenuUser from "./MenuUser";



const Menu = () => {

    const [ role, setRole ] = useState<"visitor" | "user" | "administrator">(AppStore.getState().auth.role);

    AppStore.subscribe(() => {
        setRole(AppStore.getState().auth.role)
    });

    return (
        <>
            { role === "visitor" && <MenuVisitor /> }
            { role === "user" && <MenuUser /> }
            { role === "administrator" && <MenuAdministrator /> }
        </>
    );
};

export default Menu;