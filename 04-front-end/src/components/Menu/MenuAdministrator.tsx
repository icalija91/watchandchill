import { useNavigate } from "react-router-dom";
import AppStore from "../../stores/AppStore";
import { Navbar, Nav, Container, Button, ButtonGroup, Dropdown } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

export default function MenuAdministrator() {
    const navigate = useNavigate();

    function doAdministratorLogout() {
        AppStore.dispatch( { type: "auth.reset" } );
        navigate("/auth/administrator/login");
    }

    return (
    <Navbar bg="dark" variant="dark" className="padding:10pxc">
       <Container fluid>
       <LinkContainer to="/administrator/dashboard">
                <Navbar.Brand>Watch&Chill</Navbar.Brand>
            </LinkContainer>
        <Nav className="nav-link">
            <LinkContainer to="/administrator/dashboard">
                <Nav.Link>Dashboard</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/movies">
                <Nav.Link>Movies</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/tvShows">
                <Nav.Link>TV Shows</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/administrator/tags">
                <Nav.Link>Tags</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/administrator/genres">
                <Nav.Link>Genres</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/administrator/administrators">
                <Nav.Link>Administrators</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/administrator/users">
                <Nav.Link>Users</Nav.Link>
            </LinkContainer>
          </Nav>
          <Dropdown as={ButtonGroup} className="ms-auto" variant="secondary" align="end">
             <Button variant="dark">
                 Hi,  {  AppStore.getState().auth.identity} &nbsp;&nbsp;
                 <img className="img-thumbnail rounded-circle" alt="" style={{width:50,height:50}} src="https://mdbcdn.b-cdn.net/img/new/avatars/8.webp"/>
            </Button>
            <Dropdown.Toggle split variant="dark" id="dropdown-split-basic" />
            <Dropdown.Menu>
              <Dropdown.Item onClick={doAdministratorLogout}>Logout</Dropdown.Item>
            </Dropdown.Menu>
            </Dropdown>
        </Container>
    </Navbar>
               
    );
}
