import { useNavigate } from "react-router-dom";
import AppStore from "../../stores/AppStore";
import { Navbar, Nav, Button, Container, Dropdown, ButtonGroup } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

export default function MenuUser() {

    const navigate = useNavigate();
    function doUserLogout() {
        AppStore.dispatch( { type: "auth.reset" } );
        navigate("/auth/user/login");
    }

    function doChangePassword(){
        let userId = AppStore.getState().auth.id;
       navigate(`/user/${userId}/password`);
    }

    return (
        <Navbar bg="dark" variant="dark" className="padding:10px">
           <Container fluid>
            <LinkContainer to="/">
                <Navbar.Brand>Watch&Chill</Navbar.Brand>
            </LinkContainer>
            <Nav className="nav-link">
                <LinkContainer to="/">
                    <Nav.Link>Home</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/movies">
                    <Nav.Link>Movies</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/tvShows">
                    <Nav.Link>TV Shows</Nav.Link>
                </LinkContainer>
            </Nav>
              
            <Dropdown as={ButtonGroup} className="ms-auto" variant="secondary" align="end">
             <Button variant="dark">
                 Hi,  {  AppStore.getState().auth.identity} &nbsp;&nbsp;
                 <img className="img-thumbnail rounded-circle" alt="" style={{width:50,height:50}} src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg"/>
            </Button>
            <Dropdown.Toggle split variant="dark" id="dropdown-split-basic" />
            <Dropdown.Menu>
              <Dropdown.Item onClick={doChangePassword}>Change Password</Dropdown.Item>
              <Dropdown.Item onClick={doUserLogout}>Logout</Dropdown.Item>
            </Dropdown.Menu>
            </Dropdown>
         </Container>
        </Navbar>
    );

    
}
