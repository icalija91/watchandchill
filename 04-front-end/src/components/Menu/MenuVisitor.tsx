import { Navbar, Nav, Container } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

export default function MenuVisitor() {
    return (
    <Navbar bg="dark" variant="dark" className="padding:10px">
       <Container fluid>
        <LinkContainer to="/">
            <Navbar.Brand>Watch&Chill</Navbar.Brand>
        </LinkContainer>
        <Nav className="nav-link">
            <LinkContainer to="/auth/user/login">
                <Nav.Link>User Login</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/auth/user/register">
                <Nav.Link>Register</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/auth/administrator/login">
                <Nav.Link>Admin Login</Nav.Link>
            </LinkContainer>
        </Nav>
        <div className="ms-auto"></div>
        </Container>
    </Navbar>
    );
}
