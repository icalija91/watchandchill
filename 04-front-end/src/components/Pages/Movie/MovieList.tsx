import { useEffect, useState } from 'react';
import IMovie from '../../../models/IMovie.model';
import MoviePreview from './MoviePreview';
import { api } from '../../../api/api';

export default function MovieList(){

    const [ movies, setMovies ] = useState<IMovie[]>([]);  
    const [ errorMessage, setErrorMessage ] = useState<string>("");

    useEffect(() => {
        api("get", "/api/movie", "user")
        .then(apiResponse => {
            if (apiResponse.status === 'ok'){
                return setMovies(apiResponse.data);
            }
            throw new Error("Unknown error while loading movies...");
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error while loading movies...");
        });
    }, []);

    return (
        <div>
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
            { !errorMessage && 
                    <div className="card-group">

                     {movies.map(movie => (
                       <MoviePreview key = { "Movie-" + movie.movieId } movieId = {movie.movieId}/> 
                    ))}
                    </div>
                
            }
        </div>
    );
}