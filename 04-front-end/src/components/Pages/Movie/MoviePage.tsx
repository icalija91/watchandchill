import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { api } from '../../../api/api';
import { getGenreNames, getPhotoUrl, getTagNames, isAdmin, mapStatus } from '../../../helpers/helpers';
import { Rating } from 'react-simple-star-rating'
import { Dropdown } from 'react-bootstrap';
import IMovie from '../../../models/IMovie.model';

export interface IMoviePageUrlParams extends Record<string, string | undefined>{
    id: string;
}

export interface IMovieProperties {
    movieId?: number;
}

export default function MoviePage(props: IMovieProperties) {

    const [ movie, setMovie ] = useState<IMovie|null>(null);
    const [ errorMessage, setErrorMessage ] = useState<string>("");
    const [ message,  setMessage  ] = useState<string>("");
    const [ loading, setLoading ] = useState<boolean>(false);
    const [rating, setRating] = useState(0);
    const [status, setStatus] = useState<string|null>(""); 

    const params = useParams<IMoviePageUrlParams>();

    const movieId = props?.movieId ?? params.id;

  const handleRating = (rate: number) => {
    setRating(rate);
    api("post", "/api/movie/"+ movieId + "/rate", "user", { value: rate })
    .then(res => {
        if (res.status === 'error') {
            return setErrorMessage(res.data + "");
        }

        setMessage("The rating has been saved!");
        setTimeout(() => setMessage(''), 2000);

    });
  }

  const handleStatus = (status:string|null) => {
    setStatus(status);
    api("post", "/api/movie/"+ movieId + "/status", "user", { value: status })
    .then(res => {
        if (res.status === 'error') {
            return setErrorMessage(res.data + "");
        }

        setMessage("The status has been saved!");
        setTimeout(() => setMessage(''), 2000);

    });
  };


    const loadMovie = () => {
        setLoading(true);
        api("get", "/api/movie/" + movieId, "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading movie...");
            }
            return setMovie(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }

    const loadRating = () => {
        setLoading(true);
        api("get", "/api/movie/" + movieId+'/rate', "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading rating...");
            }
            return setRating(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }

    const loadStatus = () => {
        setLoading(true);
        api("get", "/api/movie/" + movieId+'/status', "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading status...");
            }
            return setStatus(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }

    useEffect(() => {
        loadMovie();
        if(!isAdmin()){
          loadRating();
          loadStatus();
        }
    },[movieId]);

    return (

        <div>
            { loading && <p>Loading...</p> }
            { movie && (
                  <div className="row">
                    <div className="col-md-4">
                     <div className="jumbotron">
                        <h1 className="display-4">{movie.title}</h1>
                        <p className="lead"><strong>CATEGORY:</strong> {movie.category}</p>
                        <p className="lead"><strong>DIRECTOR:</strong> {movie.director}</p>
                        <p className="lead"><strong>SERBIAN TITLE:</strong> {movie.serbianTitle}</p>
                        <p className="lead"><strong>SYNOPSIS:</strong> {movie.synopsis}</p>
                        <p className="lead"><strong>TAGS:</strong> {getTagNames(movie.tagIds)}</p>
                        <p className="lead"><strong>GENRES:</strong> {getGenreNames(movie.genreIds)}</p>
                        {!isAdmin() ? (
                        <div>
                        <p className="lead"><strong>RATING:</strong> </p>
                        <Rating
                        onClick={handleRating}
                        initialValue={rating}
                        iconsCount={10} />
                        
                        <p className="lead" style={{marginTop:10}}><strong>STATUS:</strong></p>
                        <Dropdown onSelect={handleStatus}>
                         <Dropdown.Toggle variant="dark" id="dropdown-basic">
                           {mapStatus(status)}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                         <Dropdown.Item eventKey="watched">Watched</Dropdown.Item>
                         <Dropdown.Item eventKey="want_to_watch">Want to watch</Dropdown.Item>
                         <Dropdown.Item eventKey="dont_want_to_watch">Dont want to watch</Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                        </div>

                        ):null}
                     </div> 
                     { message && <div className="mt-3 alert alert-success">{ message }</div> }   
                     { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
                    </div>
                  <div className="col-md-8 how-img">
                      <img src={getPhotoUrl(movie)} className="img-fluid" style={{width:"100%"}} alt=""/>
                  </div>
              </div>             
            ) }
        </div>
    );
}