import { useState, useEffect } from 'react';
import IMovie from '../../../models/IMovie.model';
import { Link, useParams } from 'react-router-dom';
import { api } from '../../../api/api';
import { getPhotoUrl, isAdmin, truncateText } from '../../../helpers/helpers';
import ConfirmAction from '../../../helpers/ConfirmAction';

export interface IMoviePreviewUrlParams extends Record<string, string | undefined>{
    id: string;
}

export interface IMovieProperties {
    movieId?: number;
}

export default function MoviePreview(props: IMovieProperties) {

    const [ movie, setMovie ] = useState<IMovie|null>(null);
    const [ errorMessage, setErrorMessage ] = useState<string>("");
    const [ loading, setLoading ] = useState<boolean>(false);
    const [ deleteRequested, setDeleteRequested ] = useState<boolean>(false);

    const params = useParams<IMoviePreviewUrlParams>();

    const movieId = props?.movieId ?? params.id;

    const doDeleteMovie = () => {
        api("delete", "/api/movie/" + props.movieId, "administrator")
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage("Could not delete this movie!");
            }

        window.location.reload();
        })
    }

    useEffect(() => {
        setLoading(true);

        api("get", "/api/movie/" + movieId, "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading movie...");
            }
            return setMovie(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }, [ movieId ]);

    return (

        <div>
            { loading && <p>Loading...</p> }
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }

            { movie && (
              <div className="card h-100" style={{margin:5, width:"22.8rem"}}>  
                <img className="card-img-top" src={getPhotoUrl(movie)} alt="movie"/>
                <div className="card-body">
                  <h5 className="card-title">{movie?.title}</h5>
                  <p className="card-text">{truncateText(movie?.synopsis)}</p>
                </div>
                <div className="card-footer">
                <Link to={"/movies/" + movieId}  className="btn btn-light px-5 main-btn" style={{borderColor:"black",marginBottom:10}}>VIEW</Link>
                {isAdmin() ? (
                <div>
                    <Link className="btn btn-light px-5 main-btn" style={{borderColor:"black",marginBottom:10}} to={"/movies/edit/" + movieId}>EDIT</Link>
                    <Link className="btn btn-dark px-5 main-btn" to="/movies" onClick={ () => setDeleteRequested(true) }>DELETE</Link>
                    { deleteRequested && <ConfirmAction
                        title="Confirm that you want to delete this movie"
                        message={ "Are you sure that you want to delete this movie: \"" + movie.title + "\"?" }
                        onNo={ () => setDeleteRequested(false) }
                        onYes={ () => doDeleteMovie() }
                    /> }
                </div>
                ):null}
                </div>
              </div>
              
            ) }
        </div>
    );
}