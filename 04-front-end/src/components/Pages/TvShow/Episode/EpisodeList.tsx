import { useState, useEffect } from 'react';
import {  useParams } from 'react-router-dom';
import { api } from '../../../../api/api';
import IEpisode from '../../../../models/IEpisode.model';
import EpisodePreview from './EpisodePreview';

export interface IEpisodeListUrlParams extends Record<string, string | undefined>{
    id: string;
}

export default function EpisodeList() {

    const [ episodes, setEpisodes ] = useState<IEpisode[]>([]);  
    const [ errorMessage, setErrorMessage ] = useState<string>("");
    const [ loading, setLoading ] = useState<boolean>(false);

    const params = useParams<IEpisodeListUrlParams>();

    const tvShowId =  params.id;

    useEffect(() => {
        setLoading(true);
        api("get", "/api/tvShow/" + tvShowId+ "/episode", "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading episodes...");
            }
            return setEpisodes(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }, [ tvShowId ]);

    return (

        <div>
            { loading && <p>Loading...</p> }
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
            { !errorMessage && 
            <div className="card-group">

                     {episodes.map(episode => (
                       <EpisodePreview key = { "episode-" + episode.episodeId } episodeId = {episode.episodeId}/> 
                    ))}
                    </div>
            }
        </div>
    );
}