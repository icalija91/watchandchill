import { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { api } from '../../../../api/api';
import { getPhotoUrl, getTagNames, isAdmin, mapStatus } from '../../../../helpers/helpers';
import IEpisode from '../../../../models/IEpisode.model';
import { Dropdown } from 'react-bootstrap';
import { Rating } from 'react-simple-star-rating';

export interface IEpisodePageUrlParams extends Record<string, string | undefined>{
    id: string;
    eid: string;
}


export default function EpisodePage() {

    const [ episode, setEpisode ] = useState<IEpisode|null>(null);
    const [ errorMessage, setErrorMessage ] = useState<string>("");
    const [ message, setMessage ] = useState<string>("");
    const [ loading, setLoading ] = useState<boolean>(false);
    const [ status, setStatus ] = useState<string|null>("");
    const [ rating, setRating ] = useState(0);

    const params = useParams<IEpisodePageUrlParams>();

    const tvShowId =  params.id;
    const episodeId = params.eid;

    const loadEpisode = () => {
        setLoading(true);
        api("get", "/api/tvShow/" + tvShowId + "/episode/"+ episodeId, "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading episode...");
            }
            return setEpisode(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }

    const handleStatus = (status:string|null) => {
        setStatus(status);
        api("post", "/api/tvshow/"+ tvShowId + "/episode/" + episodeId + "/status", "user", { value: status })
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }
    
            setMessage("The status has been saved!");
            setTimeout(() => setMessage(''), 2000);
    
        });
      };

      const handleRating = (rate: number) => {
        setRating(rate);
        api("post", "/api/tvshow/"+ tvShowId + "/episode/" + episodeId + "/rate", "user", { value: rate })
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }
    
            setMessage("The rating has been saved!");
            setTimeout(() => setMessage(''), 2000);
    
        });
      }

      const loadStatus = () => {
        setLoading(true);
        api("get", "/api/tvshow/" + tvShowId + "/episode/" + episodeId + "/status", "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading status...");
            }
            return setStatus(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }

    const loadRating = () => {
        setLoading(true);
        api("get", "/api/tvshow/" + tvShowId + "/episode/" + episodeId + "/rate", "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading rating...");
            }
            return setRating(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }
    
    useEffect(() => {
        loadEpisode();
        if(!isAdmin()){
            loadStatus();
            loadRating();
        }
    }, [episodeId, tvShowId]);

    return (

        <div>
            { loading && <p>Loading...</p> }
            { episode && (
                  <div className="row">
                    <div className="col-md-4">
                     <div className="jumbotron">
                        <h1 className="display-4">{episode.name}</h1>
                        <p className="lead"><strong>EPISODE:</strong> {episode.number}</p>
                        <p className="lead"><strong>SEASON:</strong> {episode.seasonNumber}</p>
                        <p className="lead"><strong>TOTAL EPISODES:</strong> {episode.episodeNumber}</p>
                        <p className="lead"><strong>SYNOPSIS:</strong> {episode.synopsis}</p>
                        <p className="lead"><strong>TAGS:</strong> {getTagNames(episode.tagIds)}</p>
                        {!isAdmin() ? (
                        <div>
                        <p className="lead"><strong>RATING:</strong> </p>
                        <Rating
                        onClick={handleRating}
                        initialValue={rating}
                        iconsCount={10} />
                        
                        <p className="lead" style={{marginTop:10}}><strong>STATUS:</strong></p>
                        <Dropdown onSelect={handleStatus}>
                         <Dropdown.Toggle variant="dark" id="dropdown-basic">
                           {mapStatus(status)}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                         <Dropdown.Item eventKey="watched">Watched</Dropdown.Item>
                         <Dropdown.Item eventKey="want_to_watch">Want to watch</Dropdown.Item>
                         <Dropdown.Item eventKey="dont_want_to_watch">Dont want to watch</Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                        </div>

                        ):null}
                        <p className="lead">
                        </p>
                     </div>   
                     { message && <div className="mt-3 alert alert-success">{ message }</div> } 
                     { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
                    </div>
                  <div className="col-md-8 how-img">
                      <img src={getPhotoUrl(episode)} className="img-fluid" style={{width:"100%"}} alt=""/>
                  </div>
              </div>             
            ) }
        </div>
    );
}