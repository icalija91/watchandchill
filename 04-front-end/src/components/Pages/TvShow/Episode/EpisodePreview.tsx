import { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { api } from '../../../../api/api';
import { getPhotoUrl, isAdmin } from '../../../../helpers/helpers';
import IEpisode from '../../../../models/IEpisode.model';
import ConfirmAction from '../../../../helpers/ConfirmAction';

export interface IEpisodePreviewUrlParams extends Record<string, string | undefined>{
    id: string;
}

export interface IEpisodeProperties {
    episodeId?: number;
}

export default function EpisodePreview(props: IEpisodeProperties) {

    const [ episode, setEpisode ] = useState<IEpisode|null>(null);
    const [ errorMessage, setErrorMessage ] = useState<string>("");
    const [ loading, setLoading ] = useState<boolean>(false);
    const [ deleteRequested, setDeleteRequested ] = useState<boolean>(false);

    const params = useParams<IEpisodePreviewUrlParams>();

    const tvShowId =  params.id;
    const episodeId = props.episodeId;

    const doDeleteEpisode = () => {
        api("delete", "/api/tvshow/" + tvShowId + "/episode/" + props.episodeId, "administrator")
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage("Could not delete this episode!");
            }

        window.location.reload();
        })
    }

    useEffect(() => {
        setLoading(true);

        api("get", "/api/tvShow/" + tvShowId + "/episode/"+ episodeId, "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading episode...");
            }
            return setEpisode(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }, [episodeId, tvShowId]);

    return (

        <div>
            { loading && <p>Loading...</p> }
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }

            { episode && (
              <div className="card h-100" style={{margin:5, width:"22.8rem"}}>  
                <img className="card-img-top" src={getPhotoUrl(episode)} alt="movie"/>
                <div className="card-body">
                  <h5 className="card-title">SEASON {episode?.seasonNumber}</h5>
                  <p className="card-text">Episodes in season: {episode?.episodeNumber}</p>
                </div>
                <div className="card-footer">
                <Link to={"/tvShows/" + tvShowId+ "/episode/"+episode.episodeId}  className="btn btn-light px-5 main-btn" style={{borderColor:"black",marginBottom:10}}>EPISODE {episode.number}</Link>
                {isAdmin() ? (
                <div>
                <Link className="btn btn-light px-5 main-btn" style={{borderColor:"black",marginBottom:10}} to={"/tvShows/" + tvShowId + "/episode/edit/" + episodeId }>EDIT</Link>
                <Link className="btn btn-dark px-5 main-btn" to={"/tvShows/" + tvShowId + "/episodes"} onClick={ () => setDeleteRequested(true) }>DELETE</Link>
                { deleteRequested && <ConfirmAction
                        title="Confirm that you want to delete this episode"
                        message={ "Are you sure that you want to delete episode " + episode.episodeNumber + "?" }
                        onNo={ () => setDeleteRequested(false) }
                        onYes={ () => doDeleteEpisode() }
                    /> }
                </div>
                ):null}
                </div>
              </div>
              
            ) }
        </div>
    );
}