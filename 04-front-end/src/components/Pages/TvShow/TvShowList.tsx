import { useEffect, useState } from 'react';
import ITvShow from '../../../models/ITvShow.model';
import TvShowPreview from './TvShowPreview';
import { api } from '../../../api/api';

export default function TvShowList(){

    const [ tvShows, setTvShows ] = useState<ITvShow[]>([]);  
    const [ errorMessage, setErrorMessage ] = useState<string>("");

    useEffect(() => {
        api("get", "/api/tvshow", "user")
        .then(apiResponse => {
            if (apiResponse.status === 'ok'){
                return setTvShows(apiResponse.data);
            }
            throw new Error("Unknown error while loading tv shows...");
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error while loading tv shows...");
        });
    }, []);

    return (
        <div>
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
            { !errorMessage && 
                    <div className="card-group">

                     {tvShows.map(tvShow => (
                       <TvShowPreview key = { "tvShow-" + tvShow.tvShowId } tvShowId = {tvShow.tvShowId}/> 
                    ))}
                    </div>
                
            }
        </div>
    );
}