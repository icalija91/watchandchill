import { useState, useEffect } from 'react';
import ITvShow from '../../../models/ITvShow.model';
import { Link, useParams } from 'react-router-dom';
import { api } from '../../../api/api';
import { getPhotoUrl, getTagNames, isAdmin, mapStatus } from '../../../helpers/helpers';
import { Dropdown } from 'react-bootstrap';
import { Rating } from 'react-simple-star-rating';

export interface ITvShowPageUrlParams extends Record<string, string | undefined>{
    id: string;
}

export interface ITvShowProperties {
    tvShowId?: number;
}

export default function TvShowPage(props: ITvShowProperties) {

    const [ tvShow, setTvShow ] = useState<ITvShow|null>(null);
    const [ errorMessage, setErrorMessage ] = useState<string>("");
    const [ message, setMessage ] = useState<string>("");
    const [ loading, setLoading ] = useState<boolean>(false);
    const [ status, setStatus ] = useState<string|null>("");
    const [ rating, setRating ] = useState(0);

    const params = useParams<ITvShowPageUrlParams>();

    const tvShowId = props?.tvShowId ?? params.id;

    const handleRating = (rate: number) => {
        setRating(rate);
        api("post", "/api/tvshow/"+ tvShowId + "/rate", "user", { value: rate })
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }
    
            setMessage("The rating has been saved!");
            setTimeout(() => setMessage(''), 2000);
    
        });
      }

    const loadTvShow = () => {
        setLoading(true);
        api("get", "/api/tvshow/" + tvShowId, "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading tv show...");
            }
            return setTvShow(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }
    
    const handleStatus = (status:string|null) => {
        setStatus(status);
        api("post", "/api/tvshow/"+ tvShowId + "/status", "user", { value: status })
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage(res.data + "");
            }
    
            setMessage("The status has been saved!");
            setTimeout(() => setMessage(''), 2000);
    
        });
      };

      const loadStatus = () => {
        setLoading(true);
        api("get", "/api/tvshow/" + tvShowId + '/status', "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading status...");
            }
            return setStatus(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }

    const loadRating = () => {
        setLoading(true);
        api("get", "/api/tvshow/" + tvShowId+'/rate', "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading rating...");
            }
            return setRating(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }

    useEffect(() => {
        loadTvShow();
        if(!isAdmin()){
            loadStatus();
            loadRating();
        }
    }, [ tvShowId ]);

    return (

        <div>
            { loading && <p>Loading...</p> }
            { tvShow && (
                  <div className="row">
                    <div className="col-md-4">
                     <div className="jumbotron">
                        <h1 className="display-4">{tvShow.title}</h1>
                        <p className="lead"><strong>CATEGORY:</strong> {tvShow.category}</p>
                        <p className="lead"><strong>DIRECTOR:</strong> {tvShow.director}</p>
                        <p className="lead"><strong>SERBIAN TITLE:</strong> {tvShow.serbianTitle}</p>
                        <p className="lead"><strong>SYNOPSIS:</strong> {tvShow.synopsis}</p>
                        <p className="lead"><strong>TAGS:</strong> {getTagNames(tvShow.tagIds)}</p>
                        {!isAdmin() ? (
                        <div>
                        <p className="lead"><strong>RATING:</strong> </p>
                        <Rating
                        onClick={handleRating}
                        initialValue={rating}
                        iconsCount={10} />
                        
                        <p className="lead" style={{marginTop:10}}><strong>STATUS:</strong></p>
                        <Dropdown onSelect={handleStatus}>
                         <Dropdown.Toggle variant="dark" id="dropdown-basic">
                           {mapStatus(status)}
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                         <Dropdown.Item eventKey="watched">Watched</Dropdown.Item>
                         <Dropdown.Item eventKey="want_to_watch">Want to watch</Dropdown.Item>
                         <Dropdown.Item eventKey="dont_want_to_watch">Dont want to watch</Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                        </div>

                        ):null}
                        <p className="lead">
                        <Link to={"/tvShows/" + tvShowId + "/episodes"} className="btn btn-light px-5" style={{borderColor:"black",margin:10}}>EPISODE LIST</Link>
                        </p>
                        {isAdmin() ? (
                        <div>
                        <p className="lead">
                        <Link to={"/tvShows/" + tvShowId + "/episode/add" } className="btn btn-light px-5 " style={{borderColor:"black",margin:5}}>ADD EPISODE</Link>
                        </p>
                        </div>
                         ):null}
                     </div>  
                     { message && <div className="mt-3 alert alert-success">{ message }</div> }    
                     { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
                    </div>
                  <div className="col-md-8 how-img">
                      <img src={getPhotoUrl(tvShow)} className="img-fluid" style={{width:"100%"}} alt=""/>
                  </div>
              </div>             
            ) }
        </div>
    );
}