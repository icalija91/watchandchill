import { useState, useEffect } from 'react';
import ITvShow from '../../../models/ITvShow.model';
import { Link, useParams } from 'react-router-dom';
import { api } from '../../../api/api';
import { getPhotoUrl, isAdmin, truncateText } from '../../../helpers/helpers';
import ConfirmAction from '../../../helpers/ConfirmAction';

export interface ITvShowPreviewUrlParams extends Record<string, string | undefined>{
    id: string;
}

export interface ITvShowProperties {
    tvShowId?: number;
}

export default function TvShowPreview(props: ITvShowProperties) {

    const [ tvShow, setTvShow ] = useState<ITvShow|null>(null);
    const [ errorMessage, setErrorMessage ] = useState<string>("");
    const [ loading, setLoading ] = useState<boolean>(false);
    const [ deleteRequested, setDeleteRequested ] = useState<boolean>(false);

    const params = useParams<ITvShowPreviewUrlParams>();

    const tvShowId = props?.tvShowId ?? params.id;

    const doDeleteTvShow = () => {
        api("delete", "/api/tvshow/" + props.tvShowId, "administrator")
        .then(res => {
            if (res.status === 'error') {
                return setErrorMessage("Could not delete this tv show!");
            }

        window.location.reload();
        })
    }

    useEffect(() => {
        setLoading(true);

        api("get", "/api/tvShow/" + tvShowId, "user")
        .then(res => {
            if (res.status === 'error'){
                throw new Error("Unknown error while loading tv show...");
            }
            return setTvShow(res.data);
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error!");
        })
        .finally(() => {
            setLoading(false);
        });
    }, [ tvShowId ]);

    return (

        <div>
            { loading && <p>Loading...</p> }
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }

            { tvShow && (
              <div className="card h-100" style={{margin:5, width:"22.8rem"}}>  
                <img className="card-img-top" src={getPhotoUrl(tvShow)} alt="tvshow"/>
                <div className="card-body">
                  <h5 className="card-title">{tvShow?.title}</h5>
                  <p className="card-text">{truncateText(tvShow?.synopsis)}</p>
                </div>
                <div className="card-footer">
                <Link to={"/tvShows/" + tvShowId}  className="btn btn-light px-5 main-btn" style={{borderColor:"black",marginBottom:10}}>VIEW</Link>
                {isAdmin() ? (
                <div>
                <Link className="btn btn-light px-5 main-btn" style={{borderColor:"black",marginBottom:10}} to={"/tvShows/edit/" + tvShowId }>EDIT</Link>
                <Link className="btn btn-dark px-5 main-btn" to="/tvShows" onClick={ () => setDeleteRequested(true) }>DELETE</Link>
                { deleteRequested && <ConfirmAction
                        title="Confirm that you want to delete this tv show"
                        message={ "Are you sure that you want to delete this tv show: \"" + tvShow.title + "\"?" }
                        onNo={ () => setDeleteRequested(false) }
                        onYes={ () => doDeleteTvShow() }
                    /> }
                </div>
                ):null}
                </div>
              </div>
              
            ) }
        </div>
    );
}