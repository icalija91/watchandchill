import { useEffect, useState } from 'react';
import IMovie from '../../../models/IMovie.model';
import { api } from '../../../api/api';
import ITvShow from '../../../models/ITvShow.model';
import TvShowPreview from '../../Pages/TvShow/TvShowPreview';
import MoviePreview from '../../Pages/Movie/MoviePreview';
import AppStore from '../../../stores/AppStore';
import { useNavigate } from "react-router-dom";

export default function UserHomepage(){

    const [ movies, setMovies ] = useState<IMovie[]>([]);  
    const [ tvShows, setTvShows ] = useState<ITvShow[]>([]);  
    const [ errorMessage, setErrorMessage ] = useState<string>("");

    const navigate = useNavigate();

    useEffect(() => {
        if (AppStore.getState().auth.role === 'visitor'){
            navigate("/auth/user/login");
        }
        api("get", "/api/movie/recommended", "user")
        .then(apiResponse => {
            if (apiResponse.status === 'ok'){
                return setMovies(apiResponse.data);
            }
            throw new Error("Unknown error while loading movies...");
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error while loading movies...");
        });
        api("get", "/api/tvshow/recommended", "user")
        .then(apiResponse => {
            if (apiResponse.status === 'ok'){
                return setTvShows(apiResponse.data);
            }
            throw new Error("Unknown error while loading tv shows...");
        })
        .catch(error => {
            setErrorMessage(error?.message ?? "Unknown error while loading tv shows...");
        });
    }, []);

    return (
        <div>
            { errorMessage && <div className="mt-3 alert alert-danger" >Error: { errorMessage}</div> }
            { !errorMessage && 
            <div>
                     <h1 className="display-4" style={{margin:20}}>Recommended Movies:</h1>
                    <div className="card-group">
                     {movies.map(movie => (
                       <MoviePreview key = { "Movie-" + movie.movieId } movieId = {movie.movieId}/> 
                    ))}
                    </div> 

                  <h1 className="display-4" style={{margin:20}}>Recommended TV Shows:</h1>
                  <div className="card-group">

                    {tvShows.map(tvShow => (
                        <TvShowPreview key = { "tvShow-" + tvShow.tvShowId } tvShowId = {tvShow.tvShowId}/> 
                        ))}
                    </div> 
            </div> 
            }
        </div>
    );
}