import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { api } from "../../../api/api";

export default function UserRegisterPage(){

    const [ email, setEmail ] = useState<string>("");
    const [ password, setPassword ] = useState<string>("");
    const [ username, setUsername ] = useState<string>("");
    const [ error, setError ] = useState<string>("");

    const navigate = useNavigate();

    const doRegister = () => {
        api("post", "/api/user/register", "user", { email, password, username })
        .then(res => {
            if (res.status !== "ok") {
                throw new Error("Could not register your account. Reason: " + JSON.stringify(res.data));
            }
        })
        .then(data => {
            navigate("/auth/user/login", {
                replace: true,
            });
        })
        .catch(error => {
            setError(error?.message ?? "Could not register your account!");

            setTimeout(() => {
                setError("");
            }, 3500);
        });
    };
    
    return (
        <div className="div-center">
                <h1 className="h5 mb-3 center">Register</h1>

                <div className="form-group mb-3">
                    <div className="input-group">
                        <input className="form-control" type="text" placeholder="Enter your email" value={ email }
                            onChange={ e => setEmail(e.target.value)}/>
                    </div>
                </div>

                <div className="form-group mb-3">
                    <div className="input-group">
                        <input className="form-control" type="text" placeholder="Enter your username" value={ username }
                            onChange={ e => setUsername(e.target.value)}/>
                    </div>
                </div>

                <div className="form-group mb-3">
                    <div className="input-group">
                        <input className="form-control" type="password" placeholder="Enter your password" value={ password }
                             onChange={ e => setPassword(e.target.value)}/>
                    </div>
                </div>

                <div className="form-group mb-3">
                    <button className="btn btn-dark px-5 main-btn" onClick= { () =>  doRegister() }>
                        REGISTER
                    </button>
                </div>
                { error && <p className="alert alert-danger">{error}</p> }
        </div>

    );
}