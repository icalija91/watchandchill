import IGenre from "../models/IGenre.model";
import ITag from "../models/ITag.model";
import AppStore from "../stores/AppStore";

export function getPhotoUrl(element: { imageUrl: string }) {
  if (element?.imageUrl !== "") {
    return element?.imageUrl;
  }
  return process.env.PUBLIC_URL + "/placeholder.png";
}

export function getTagNames(tags: ITag[] | undefined) {
  let names: any = tags?.map((obj) => obj.name);
  let elements = names?.join(", ");
  return elements;
}

export function getGenreNames(genres: IGenre[] | undefined) {
  let names: any = genres?.map((obj) => obj.name);
  let elements = names?.join(", ");
  return elements;
}

export function isAdmin() {
  let role = AppStore.getState().auth.role;
  return role === "administrator";
}

export function mapStatus(status: string | null) {
  if (status === null) {
    return "Select status";
  } else if (status === "watched") {
    return "Watched";
  } else if (status === "want_to_watch") {
    return "Want to watch";
  }
  return "Dont want to watch";
}

export function truncateText(synopsis: string) {
  if (synopsis.length > 200) {
    return synopsis.substring(0, 200) + "...";
  }
  return synopsis;
}