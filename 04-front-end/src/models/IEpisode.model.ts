import ITag from "./ITag.model";

export default interface IEpisode {

    tagIds?: ITag[];
    episodeId: number;
    name: string;
    number: number;
    synopsis: string;
    imageUrl: string;
    seasonNumber : number;
    episodeNumber : number;
    tvShowId : number;
    
}