import IGenre from "./IGenre.model";
import ITag from "./ITag.model";

export default interface IMovie {
    movieId: number;
    title: string;
    serbianTitle: string;
    director: string;
    synopsis: string;
    imageUrl: string;
    category: 'artistic' | 'indie' | 'box-office' | 'documentary' | 'silent';
    tagIds?: ITag[];
    genreIds?: IGenre[];
}