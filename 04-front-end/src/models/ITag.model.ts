export default interface ITag {

    tagId: number;
    name: string;

}