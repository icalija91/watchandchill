import IEpisode from "./IEpisode.model";
import ITag from "./ITag.model";

export default interface ITvShow {
    tvShowId: number;
    title: string;
    serbianTitle: string;
    director: string;
    synopsis: string;
    imageUrl: string;
    category: 'artistic' | 'indie' | 'box-office' | 'documentary' | 'silent';
    tagIds?: ITag[];
    episodes?: IEpisode[];
}