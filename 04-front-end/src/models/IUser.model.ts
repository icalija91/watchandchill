export default interface IUser {
    userId: number;
    username: string;
    email: string;
    passwordHash: string|null;
    activationCode: string|null;
    passwordResetCode: string|null;
}
